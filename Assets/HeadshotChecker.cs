﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class HeadshotChecker : MonoBehaviour
{
	void OnTriggerEnter2D(Collider2D coll){
		if (coll.gameObject.tag == "Arrow") {
			GameScene.instance.HitHeadshot(this.transform.position);
		}
		Debug.LogError ("Trigger Enter");
	}
}
