﻿using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine.EventSystems;

public class ZNotificationMgr : ZSingleton<ZNotificationMgr> {
	
	public void AddNotification(string text, int addMinutes)
	{
		ScheduleNotificationForiOSWithMessage (text, DateTime.Now.AddMinutes(addMinutes));
	}

	public void ClearNotifications()
	{
		#if UNITY_IOS
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			UnityEngine.iOS.NotificationServices.CancelAllLocalNotifications ();
		}
		#endif
	}

	void ScheduleNotificationForiOSWithMessage (string text, System.DateTime fireDate)
	{
		#if UNITY_IOS
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			UnityEngine.iOS.LocalNotification notification = new UnityEngine.iOS.LocalNotification ();
			notification.fireDate = fireDate;
			notification.alertAction = "Alert";
			notification.alertBody = text;
			notification.hasAction = false;

			UnityEngine.iOS.NotificationServices.ScheduleLocalNotification(notification);

			UnityEngine.iOS.NotificationServices.RegisterForNotifications (UnityEngine.iOS.NotificationType.Alert | UnityEngine.iOS.NotificationType.Badge);
		}        
		#endif
	}

	/*public void RegisterNotification()
	{
	}*/

	public void StartNotification()
	{/*
		UnityEngine.iOS.LocalNotification n = new UnityEngine.iOS.LocalNotification();
		n.fireDate = DateTime.Now.AddSeconds(10);
		n.alertAction = "Title Here";
		n.alertBody = "My Alert Body Text";
		n.applicationIconBadgeNumber = 99;
		n.hasAction = true;//?
		n.repeatCalendar = UnityEngine.iOS.CalendarIdentifier.GregorianCalendar;
		n.repeatInterval = UnityEngine.iOS.CalendarUnit.Day;
		n.soundName = UnityEngine.iOS.LocalNotification.defaultSoundName;
		UnityEngine.iOS.NotificationServices.ScheduleLocalNotification (n);*/
	}
}
