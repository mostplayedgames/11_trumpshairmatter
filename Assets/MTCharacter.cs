﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum MTCHAR_STATES
{
	IDLE,
	RUN,
	RUNSTRAIGHT,
	STEADY,
	SWOOP,
	SWOOPUP,
	POOPRUN,
	DIE,
	SUCCESS
}

[System.Serializable]
public class SFXHandler{
	public List<AudioClip> m_listAudio;
	public void PlaySFX(){
		int randomSFX = Random.Range (0, m_listAudio.Count - 1);
		ZAudioMgr.Instance.PlaySFX (m_listAudio [randomSFX]);
	}
}

public class MTCharacter : MonoBehaviour {

	public static MTCharacter instance;

	public GameObject m_spriteObject;
	public GameObject m_objectParticle;
	public GameObject m_objectParticleChar;
	public GameObject m_rootParticle;

	public AudioClip m_audioSwoosh;

	public int m_currentMode = 0;

	public GameObject m_prefabPoop;
	public GameObject m_rootPoop;

	public SFXHandler m_sfxPoopSpawn;
	public SFXHandler m_sfxPoopDie;

	public MTCHAR_STATES m_currentState;
	MTCHAR_STATES m_nextState;

	public GameObject m_objectHairRoot;

	float m_moveSpeed = 5;
	float m_moveSpeedVertical = 3;
	float m_moveDirection = 1;
	float m_moveDirectionVertical = 1;
	public float m_defaultScale = 0.8f;
	float m_speedMultiplier = 1;

	float m_swoopSpeed = -22;
	float m_maxFlight = 9f;

	float m_minSpawnRange = 0;
	float m_maxSpawnRange = 10f;

	float m_maxSpeed = 1.5f;

	bool m_hasSpawned = false;
	bool m_hasPooped = false;

	float m_targetVertical;
	float m_targetVerticalMax;

	// Use this for initialization
	void Start () {
		m_moveDirection = 1;
		m_moveDirectionVertical = -1;

		instance = this;
		m_nextState = MTCHAR_STATES.RUN;
		m_hasSpawned = false;
		m_hasPooped = false;

		ZObjectMgr.Instance.AddNewObject (m_prefabPoop.name, 20, m_rootPoop);

		//Run ();
	}

	// Update is called once per frame
	void Update () {
		switch (m_currentState) {
		case MTCHAR_STATES.IDLE:
			break;
		case MTCHAR_STATES.POOPRUN:
			if (m_moveDirectionVertical == 1) {
				MoveUpPoop ();
			} else if (m_moveDirectionVertical == -1) {
				MoveDownPoop ();
			}

			break;
		case MTCHAR_STATES.RUN:
			if (m_moveDirection == 1)
				MoveLeft ();
			else
				MoveRight ();

			if (m_moveDirectionVertical == 1)
				MoveUp ();
			else
				MoveDown ();
			break;
		case MTCHAR_STATES.RUNSTRAIGHT:
			if (m_moveDirection == 1)
				MoveLeft ();
			else
				MoveRight ();
			
			if (m_moveDirectionVertical == 1)
				MoveUp ();
			else
				MoveDown ();
			break;
		case MTCHAR_STATES.DIE:
			break;
		case MTCHAR_STATES.SWOOP:
			this.GetComponent<Rigidbody2D> ().velocity = new Vector3 (0, m_swoopSpeed, 0);

			break;
		case MTCHAR_STATES.SWOOPUP:
			this.GetComponent<Rigidbody2D> ().velocity = new Vector3 (0, -m_swoopSpeed, 0);

			if (this.transform.localPosition.y > m_maxFlight) {
				if (GameScene.instance.GetLevel () > 3 && m_nextState == MTCHAR_STATES.RUN) {
					Run (m_maxSpeed);
				} else {
					RunStraight (m_maxSpeed);
				}
			}

			break;
		case MTCHAR_STATES.SUCCESS:
			if (m_moveDirection == 1)
				MoveLeft ();
			else
				MoveRight ();

			if (m_moveDirectionVertical == 1)
				MoveUp ();
			else
				MoveDown ();
			break;
		}
	}

	public void Poop(){
		if (m_currentState == MTCHAR_STATES.DIE)
			return;
		
		GameObject obj = ZObjectMgr.Instance.Spawn2D (m_prefabPoop.name, m_objectHairRoot.transform.position);
		obj.transform.parent = m_rootPoop.transform;
		obj.transform.localEulerAngles = Vector3.zero;
		obj.GetComponent<BoxCollider2D> ().enabled = true;
		obj.GetComponent<Rigidbody2D> ().isKinematic = false;
		obj.transform.position = m_objectHairRoot.transform.position;
		obj.transform.localScale = new Vector3 (0.7f, 0.7f, 0.7f);

		GameScene.instance.m_objectBall = obj;
		//RandomSwoop();
		//PoopRun();
		m_sfxPoopSpawn.PlaySFX ();
	}

	public void Success()
	{
		if (m_currentState == MTCHAR_STATES.SUCCESS)
			return;
		this.transform.localEulerAngles = new Vector3 (0, 0, 0);
		this.transform.localScale = new Vector3 (m_defaultScale, m_defaultScale, m_defaultScale);

		m_speedMultiplier = Random.Range (1f, 2f);
		int randomRun = Random.Range (0, 100);
		if( randomRun > 50 )
			m_moveDirectionVertical = 1;
		else
			m_moveDirectionVertical = -1;
		m_currentState = MTCHAR_STATES.SUCCESS;
	}

	public void PoopDie()
	{
		m_sfxPoopDie.PlaySFX ();
	}

	public void PoopRun(float forceVertical = -999)
	{
		if (forceVertical == -999) {
			if( GameScene.instance.GetLevel() > 25 )
				m_targetVertical = Random.Range (-1, 10f);
			else if( GameScene.instance.GetLevel() > 10 )
				m_targetVertical = Random.Range (-1, 9f);
			else if( GameScene.instance.GetLevel() > 5 )
				m_targetVertical = Random.Range (-1, 8f);
			else
				m_targetVertical = Random.Range (-1, 7f);
		} else {
			m_targetVertical = forceVertical;
		}
		//m_targetVerticalMax = Random.Range (6f, 9f);

		if (this.transform.localPosition.y < m_targetVertical)
			m_moveDirectionVertical = 1;
		else if (this.transform.localPosition.y > m_targetVertical)
			m_moveDirectionVertical = -1;

		
		/*if (this.transform.position.y < m_targetVertical) {
			m_moveDirectionVertical = 1;
		}
		else {
			m_moveDirectionVertical = -1;
		}*/

		m_currentState = MTCHAR_STATES.POOPRUN;
	}

	public void SetMaxSpeed(float maxSpeed)
	{
		m_maxSpeed = maxSpeed;
		if (m_currentMode == 1) {
			m_maxSpeed *= 0.8f;
		}
	}

	public void SetDirection(int direction){
		this.transform.localScale = new Vector3 (m_defaultScale * direction, m_defaultScale, m_defaultScale);
	}

	public void Run(float maxSpeed = 1)
	{
		if (m_currentState == MTCHAR_STATES.RUN)
			return;
		this.transform.localEulerAngles = new Vector3 (0, 0, 0);
		this.transform.localScale = new Vector3 (m_defaultScale, m_defaultScale, m_defaultScale);

		m_speedMultiplier = Random.Range (1, maxSpeed);
		int randomRun = Random.Range (0, 100);
		if( randomRun > 50 )
			m_moveDirectionVertical = 1;
		else
			m_moveDirectionVertical = -1;
		
		m_currentState = MTCHAR_STATES.RUN;
	}

	public void RunStraight(float maxSpeed = 1)
	{
		if (m_currentState == MTCHAR_STATES.RUNSTRAIGHT)
			return;
		this.transform.localEulerAngles = new Vector3 (0, 0, 0);
		this.transform.localScale = new Vector3 (m_defaultScale, m_defaultScale, m_defaultScale);

		m_speedMultiplier = Random.Range (1, maxSpeed);
		m_moveDirectionVertical = 0;

		m_currentState = MTCHAR_STATES.RUNSTRAIGHT;
	}

	public void Reset()
	{
		this.GetComponent<Rigidbody2D> ().isKinematic = false;

		/*if (m_currentMode != 3) {
			if (m_currentState == MTCHAR_STATES.DIE) {
				if (m_currentMode != 3)
					SwoopUpReset ();
				else
					m_currentState = MTCHAR_STATES.RUN;
			} else {
				RunStraight (1);
				m_maxFlight = 4f;
			}
		} else {
			*/PoopRun (3f);
		//}

		m_hasSpawned = false;

		this.GetComponent<CircleCollider2D> ().enabled = true;
		m_spriteObject.GetComponent<Animator> ().StopPlayback ();
		m_objectParticle.SetActive (false);
	}

	public void Swoop()
	{
		if (m_currentState == MTCHAR_STATES.DIE)
			return;
		
		if (m_currentState == MTCHAR_STATES.SWOOP)
			return;
		
		ZAudioMgr.Instance.PlaySFX (m_audioSwoosh);
		this.transform.localEulerAngles = new Vector3 (0, 0, -90);
		this.transform.localScale = new Vector3 (m_defaultScale, m_defaultScale, m_defaultScale);
		//this.GetComponent<Rigidbody2D> ().velocity = Vector3.zero;
		//this.GetComponent<Rigidbody2D> ().gravityScale = 4f;
		m_currentState = MTCHAR_STATES.SWOOP;
	}

	public void SetSpawnRange(float minRange, float maxRange)
	{
		m_minSpawnRange = minRange;
		m_maxSpawnRange = maxRange;
	}

	public void RandomSwoop()
	{
		/*int randomType = Random.Range (0, 100);
		if (randomType > 40) {
			m_maxFlight = Random.Range (m_maxSpawnRange - 2f, m_maxSpawnRange);*/
			Run (m_maxSpeed);
		/*} else {
			m_maxFlight = Random.Range (m_minSpawnRange, m_minSpawnRange + 3f);
			RunStraight (m_maxSpeed);
		}*/
	}

	public void SwoopUp()
	{
		if (m_currentState == MTCHAR_STATES.SWOOPUP)
			return;

		int randomType = Random.Range (0, 100);
		if (randomType > 50) {
			m_moveDirectionVertical = -1;
			m_nextState = MTCHAR_STATES.RUN;
		}
		else if (randomType > 30) {
			m_nextState = MTCHAR_STATES.RUNSTRAIGHT;
			m_maxFlight = Random.Range (m_maxSpawnRange - 2f, m_maxSpawnRange);
		}
		else {
			m_nextState = MTCHAR_STATES.RUNSTRAIGHT;
			m_maxFlight = Random.Range (m_minSpawnRange, m_minSpawnRange + 3f);
		}

		if (m_currentMode == 1) {
			//if(m_maxFlight >= 3f) {
				//m_maxFlight = 3f;
			//}
			m_maxFlight = Random.Range (m_minSpawnRange, 6f);
			m_nextState = MTCHAR_STATES.RUNSTRAIGHT;
		}

		this.GetComponent<Rigidbody2D> ().gravityScale = 0f;

		ZAudioMgr.Instance.PlaySFX (m_audioSwoosh);
		this.transform.localEulerAngles = new Vector3 (0, 0, 90);
		this.transform.localScale = new Vector3 (m_defaultScale, m_defaultScale, m_defaultScale);
		m_currentState = MTCHAR_STATES.SWOOPUP;
	}

	public void SwoopUpReset()
	{
		if (m_currentState == MTCHAR_STATES.SWOOPUP)
			return;

		m_nextState = MTCHAR_STATES.RUNSTRAIGHT;
		m_maxFlight = 4f;

		this.transform.localEulerAngles = new Vector3 (0, 0, 90);
		this.transform.localScale = new Vector3 (m_defaultScale, m_defaultScale, m_defaultScale);
		m_currentState = MTCHAR_STATES.SWOOPUP;
	}

	public void Die()
	{
		if (m_currentState == MTCHAR_STATES.DIE)
			return;

		this.GetComponent<Rigidbody2D> ().isKinematic = true;
		this.GetComponent<CircleCollider2D> ().enabled = false;

		m_currentState = MTCHAR_STATES.DIE;

		if( m_currentMode == 0 || m_currentMode == 2 )
			m_spriteObject.GetComponent<Animator> ().StartPlayback ();

		m_objectParticleChar.GetComponent<ParticleSystem> ().Clear ();
		m_objectParticleChar.transform.position = m_rootParticle.transform.position;
		m_objectParticleChar.SetActive (true);
		if( m_currentMode == 0 || m_currentMode == 2 )
			m_objectParticleChar.GetComponent<ParticleSystem> ().Play ();
	}

	void MoveLeft()
	{
		this.GetComponent<Rigidbody2D> ().velocity = new Vector3 ( -m_moveSpeed * m_speedMultiplier, m_moveSpeedVertical * m_moveDirectionVertical * m_speedMultiplier, 0);
		this.transform.localScale = new Vector3 (-m_defaultScale, m_defaultScale, m_defaultScale);

		if (this.transform.localPosition.x < -5.6f) {
			m_moveDirection = -1;
			ZAudioMgr.Instance.PlaySFX (m_audioSwoosh);
			if (!m_hasSpawned && m_currentMode == 1 && GameScene.instance.m_eState != GAME_STATE.RESULTS) {
				if (GameScene.instance.m_eState != GAME_STATE.START) {
					GameScene.instance.SpawnEnemy (1);
					m_hasSpawned = true;
				}
			}
			if (m_currentMode == 1) {
				RunStraight (m_maxSpeed);
			}
		}
	}

	void MoveRight()
	{
		this.GetComponent<Rigidbody2D> ().velocity = new Vector3 ( m_moveSpeed * m_speedMultiplier, m_moveSpeedVertical * m_moveDirectionVertical * m_speedMultiplier, 0);
		this.transform.localScale = new Vector3 (m_defaultScale, m_defaultScale, m_defaultScale);

		if (this.transform.localPosition.x > 5.6f) {
			m_moveDirection = 1;
			ZAudioMgr.Instance.PlaySFX (m_audioSwoosh);
			if (!m_hasSpawned && m_currentMode == 1 && GameScene.instance.m_eState != GAME_STATE.RESULTS) {
				if (GameScene.instance.m_eState != GAME_STATE.START) {
					GameScene.instance.SpawnEnemy (-1);
					m_hasSpawned = true;
				}
			}
			if (m_currentMode == 1) {
				RunStraight (m_maxSpeed);
			}
		}
	}

	void MoveDown()
	{
		if (this.transform.localPosition.y < -1f) {
			m_moveDirectionVertical = 1;
			ZAudioMgr.Instance.PlaySFX (m_audioSwoosh);
		}
	}

	void MoveUp()
	{
		if (this.transform.localPosition.y > 9f) {
			m_moveDirectionVertical = -1;
			ZAudioMgr.Instance.PlaySFX (m_audioSwoosh);
		}
	}

	void MoveDownPoop()
	{
		
		if (this.transform.localPosition.y < m_targetVertical) {
			m_moveDirectionVertical = 0;
			ZAudioMgr.Instance.PlaySFX (m_audioSwoosh);
			this.GetComponent<Rigidbody2D> ().velocity = new Vector3 ( 0, 0, 0);
		} else {
			this.GetComponent<Rigidbody2D> ().velocity = new Vector3 ( 0, -10f, 0);
		}
	}

	void MoveUpPoop()
	{
		
		if (this.transform.localPosition.y > m_targetVertical) {
			m_moveDirectionVertical = 0;
			ZAudioMgr.Instance.PlaySFX (m_audioSwoosh);
			this.GetComponent<Rigidbody2D> ().velocity = new Vector3 ( 0, 0, 0);
		} else {
			this.GetComponent<Rigidbody2D> ().velocity = new Vector3 ( 0, 10f, 0);
		}
	}

	public void AnimateDeathParticle(GameObject enemyObject)
	{
		m_objectParticle.GetComponent<ParticleSystem> ().Clear ();
		m_objectParticle.transform.position = enemyObject.transform.position;
		m_objectParticle.SetActive (true);
		m_objectParticle.GetComponent<ParticleSystem> ().Play ();
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (m_currentState == MTCHAR_STATES.DIE)
			return;

		if (GameScene.instance.m_eState == GAME_STATE.RESULTS)
			return;
		
		if (coll.gameObject.tag == "Die") {
			GameScene.instance.Die ();
			//Die ();
		}
		else if (coll.gameObject.tag == "Enemy") {
			SwoopUp();
			if (((coll.gameObject.GetComponent<MTEnemy>().m_moveDirection > 0 && this.transform.position.x < coll.gameObject.transform.position.x + 0.5f &&
				this.transform.position.x > coll.gameObject.transform.position.x - 0.05f ) || 
				(coll.gameObject.GetComponent<MTEnemy>().m_moveDirection < 0 && this.transform.position.x > coll.gameObject.transform.position.x - 0.5f &&
					this.transform.position.x < coll.gameObject.transform.position.x + 0.05f ))){
				GameScene.instance.Score (2);
				GameScene.instance.HitHeadshot (this.transform.position);
			}else{
				GameScene.instance.Score (1);
			}

			m_objectParticle.GetComponent<ParticleSystem> ().Clear ();
			m_objectParticle.transform.position = m_rootParticle.transform.position;
			m_objectParticle.SetActive (true);
			m_objectParticle.GetComponent<ParticleSystem> ().Play ();

			m_hasSpawned = false;
		}
	}
}
