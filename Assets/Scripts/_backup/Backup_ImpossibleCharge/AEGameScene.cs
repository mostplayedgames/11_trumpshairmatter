﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

//using ChartboostSDK;

public enum AEGAME_STATE
{
	IDLE,
	HOLD,
	RESULTS,
}

[System.Serializable]
public class AELevelData
{
	public int max;
	public int target;
	public int range;
	public float speed;
	LeanTweenType type;
}

public class AEGameScene : ZGameScene {

	public static AEGameScene instance;

	public TextAsset m_csvLevelData;

	public GameObject m_prefabCake;
	public GameObject m_rootTutorial;

	public AudioClip m_audioDie;

	public Text m_textScore;
	public Text m_textLevel;
	public Text m_textMax;
	public Text m_textTarget;
	public Text m_textDescription;
	public Text m_textPerfect;
	public Text m_textTries;

	public GameObject m_objectCamera;
	public GameObject m_objectDefault;

	List<AELevelData> m_listLevelData;

	public Color m_colorSuccess;
	public Color m_colorFail;
	public Color m_colorNormal;
	public Color m_colorText;
	public Color m_colorSwitch;

	public GameObject m_objectBattery;
	public GameObject m_objectSkip;
	public GameObject m_rootBatteryGroup;

	public AudioClip m_audioCharge;
	public AudioClip m_audioWin;
	public AudioClip m_audioFail;
	public AudioClip m_audioChange;

	public List<Color> m_listBackgroundColors;

	GameObject m_currentCake;
	int m_currentScore;
	int m_currentLevel;
	int m_currentAttempts;

	AEGAME_STATE m_eState;

	int m_currentMax;
	int m_currentTarget;
	int m_currentRange;
	float m_currentSpeed;

	int m_currentTries;

	bool m_isSuccess;

	void Awake()
	{
		instance = this;
	}

	void Start()
	{
		m_eState = AEGAME_STATE.IDLE;

		ZAudioMgr.Instance.enabled = true;

		//ZObjectMgr.Instance.AddNewObject (m_prefabCake.name, 100, this.gameObject);
		ZAdsMgr.didCompleteRewardedVideo += DidCompleteRewardedVideo;

		m_currentScore = 0;
		m_textScore.text = "" + m_currentScore;
		m_currentTries = 0;
		m_currentLevel = 0;

		m_currentLevel = PlayerPrefs.GetInt ("Level");
		m_currentAttempts = PlayerPrefs.GetInt ("Tries");

		//CenterCamera (m_objectDefault.transform.position);

		//SpawnCake ();
		LoadData ();
		SetupLevel ();
		//CSVReader.DebugOutputGrid( CSVReader.SplitCsvGrid(m_csvLevelData.text) ); 
		// + " - GET " + m_currentTarget;
	}

	void Update()
	{
		//ZAudioMgr.Instance.PlaySFX (m_audioDie);
		//m_textScore.text = "" + Mathf.FloorToInt(((m_objectDefault.transform.localScale.x / 2f)) * m_currentMax);
	}

	void LoadData()
	{
		m_listLevelData = new List<AELevelData> ();
		string[] levelDataList = CSVReader.SplitCsvLine (m_csvLevelData.text); 

		AELevelData data = new AELevelData();

		for(int x = 0; x < levelDataList.Length; x++)
		{
			if( x > 10 )
			{	if( (x+1) % 10 == 0 )
				{	Debug.Log("Next Line");
					m_listLevelData.Add(data);
				}
				else if( (x+1) % 10 == 3 )
				{	Debug.Log("Target " + levelDataList[x]);
					data.target = int.Parse(levelDataList[x]);
				}
				else if( (x+1) % 10 == 4 )
				{	Debug.Log("Range " + levelDataList[x]);
					data.range = int.Parse(levelDataList[x]);
				}
				else if( (x+1) % 10 == 5 )
				{	Debug.Log("Speed " + levelDataList[x]);
					data.speed = int.Parse(levelDataList[x]);
				}
				else if( (x+1) % 10 == 6 )
				{	Debug.Log("Max " + levelDataList[x]);
					data.max = int.Parse(levelDataList[x]);
				}
				else if( (x+1) % 10 == 2 )
				{	data = new AELevelData();
				}
			}

		}
		m_listLevelData.Add(data);
	}

	void SetupLevel()
	{
		AELevelData data = m_listLevelData [m_currentLevel];

		m_currentMax = data.max;
		m_currentTarget = data.target;
		m_currentSpeed = data.speed;
		m_currentRange = data.range;

		m_objectCamera.GetComponent<Camera> ().backgroundColor = m_colorNormal;

		m_objectDefault.transform.localScale = new Vector3 (0, 0.5f, 1);
		m_textMax.text = "Max " + m_currentMax;
		//m_textTarget.text = "CHARGE BY " + m_currentTarget;
		if (m_currentRange > 0) {
			m_textTarget.text = "" + (m_currentTarget-m_currentRange) + "-" + (m_currentTarget+m_currentRange);
		} else {
			m_textTarget.text = "" + m_currentTarget;
		}

		m_textLevel.text = "Level " + (m_currentLevel+1);
		m_textDescription.text = "HOLD AND RELEASE";
		m_isSuccess = false;

		m_textLevel.color = m_colorText;
		if( m_currentAttempts > 10 )
			m_textTries.text = "TRIES " + m_currentAttempts;
		else
			m_textTries.text = "";

		/* Remove Attempts for Now
		if( m_currentAttempts > 15 )
		{	ShowSkip();
		}
		else
		{	m_objectSkip.SetActive (false);
		}*/

		m_eState = AEGAME_STATE.IDLE;

		ZAudioMgr.Instance.PlaySFX (m_audioChange);
		m_textScore.text = "";
		m_textPerfect.text = "";
		m_textDescription.gameObject.transform.localScale = new Vector3 (0.8f, 0.8f, 0.8f);
		LeanTween.scale (m_textDescription.gameObject, new Vector3(0.9f,0.9f,0.9f), 0.2f).setEase (LeanTweenType.easeInOutQuad).setLoopPingPong ().setLoopCount (2);

		/* Remove Getting of Rewarded Video for Now
 
		if (!ZAdsMgr.Instance.HasRewardedVideoAd ()) {
			ZAdsMgr.Instance.RequestRewardedVideoAd ();
		}*/
	}

	void SwitchBattery()
	{
		LeanTween.moveLocalX (m_objectBattery, -15f, 1f).setEase (LeanTweenType.easeInElastic).setOnComplete(SwitchBatteryRoutine);

		m_textDescription.text = "";
		m_textTarget.text = "";
		//m_textMax.text = "";
		m_textTries.text = "";
		m_textScore.text = "";
		m_textPerfect.text = "";
		m_textLevel.color = m_colorSwitch;

		m_textLevel.text = "LEVEL " + m_currentLevel;		//ZAudioMgr.Instance.PlaySFX (m_audioChange);
	}

	void SwitchBatteryRoutine()
	{
		m_objectDefault.transform.localScale = new Vector3 (0, 0.5f, 1);
		m_objectBattery.transform.localPosition = new Vector3 (15f, -1, 0);
		LeanTween.moveLocalX (m_objectBattery, 0, 1f).setEase (LeanTweenType.easeOutElastic).setOnComplete (SetupLevel);

		ZAudioMgr.Instance.PlaySFX (m_audioChange);

		m_textLevel.text = "LEVEL " + (m_currentLevel+1);
	}

	void Success()
	{
		m_objectCamera.GetComponent<Camera> ().backgroundColor = m_colorSuccess;

		m_currentLevel++;
		LeanTween.delayedCall (1f, SwitchBattery);

		LeanTween.scale (m_textPerfect.gameObject, new Vector3(1.5f,1.5f,1.5f), 0.2f).setEase (LeanTweenType.easeInOutQuad).setLoopPingPong ().setLoopCount (2);

		//m_textTarget.text = "PERFECT!";
		m_textPerfect.text = "PERFECT!";
		m_textLevel.color = new Color (0f, 0f, 0f, 0.3f);

		//m_eState = GAME_STATE.RESULTS;

		ZAudioMgr.Instance.PlaySFX (m_audioWin);

		LeanTween.delayedCall (0.2f, ShowPressAnyKey);

		PlayerPrefs.SetInt ("Level", m_currentLevel);
		m_isSuccess = true;

		m_currentAttempts = 0;
		PlayerPrefs.SetInt ("Tries", m_currentAttempts);

		ZAdsMgr.Instance.ShowInsterstitial ();


		if( m_currentLevel > 5 && m_currentLevel % 25 == 0 )
		{	ZRateUsMgr.Instance.ShowRateUs();
		}
		/*
		if (m_currentTries > 3) {
			ZAdsMgr.Instance.ShowInsterstitial ();
			m_currentTries = 0;
		}
		else
		{	m_currentTries++;
		}*/
	}

	void Fail()
	{

		m_objectCamera.GetComponent<Camera> ().backgroundColor = m_colorFail;
		//LeanTween.delayedCall (1.5f, SetupLevel);

		//m_textTarget.text = "FAIL!";
		//m_textLevel.text = "FAIL!";
		m_textPerfect.text = "FAIL!";

		//LeanTween.scale (m_textPerfect.gameObject, new Vector3(1.2f,1.2f,1.2f), 0.2f).setEase (LeanTweenType.easeInOutQuad).setLoopPingPong ().setLoopCount (2);
		LeanTween.rotateZ(m_rootBatteryGroup, 3, 0.1f).setEase(LeanTweenType.easeInOutCubic).setLoopPingPong().setLoopCount(2);

		m_eState = AEGAME_STATE.RESULTS;

		LeanTween.delayedCall (1f, ShowPressAnyKey);

		ZAudioMgr.Instance.PlaySFX (m_audioFail);

		m_currentAttempts++;
		PlayerPrefs.SetInt ("Tries", m_currentAttempts);
	}

	void ShowPressAnyKey()
	{
		if (m_eState != AEGAME_STATE.RESULTS)
			return;

		m_textDescription.text = "TAP TO CONTINUE";
		m_textDescription.gameObject.transform.localScale = new Vector3 (0.8f, 0.8f, 0.8f);
		LeanTween.scale (m_textDescription.gameObject, new Vector3(0.9f,0.9f,0.9f), 0.2f).setEase (LeanTweenType.easeInOutQuad).setLoopPingPong ().setLoopCount (2);
	}


	bool m_isResults;


	void HideTutorial()
	{
		m_rootTutorial.SetActive (false);
	}

	void ExpandGraph()
	{
		if (m_isSuccess)
			return;

		if (m_eState == AEGAME_STATE.RESULTS)
			return;

		LeanTween.scale (m_objectDefault, new Vector3 (2, 0.5f, 1), m_currentSpeed);//.setEase(LeanTweenType.easeInQuad);
		m_rootTutorial.SetActive (false);

		ZAudioMgr.Instance.PlaySFX (m_audioCharge, true, 0.9f);
		m_textDescription.text = "";



		m_eState = AEGAME_STATE.HOLD;
	}

	void StopGraph()
	{
		if (m_isSuccess)
			return;

		if (m_eState != AEGAME_STATE.HOLD)
			return;

		LeanTween.cancel (m_objectDefault);

		ZAudioMgr.Instance.StopSFX (m_audioCharge);
		m_textScore.text = "" + Mathf.FloorToInt(((m_objectDefault.transform.localScale.x / 2f)) * m_currentMax);

		int score = Mathf.FloorToInt (((m_objectDefault.transform.localScale.x / 2f)) * m_currentMax);
		if( score == m_currentTarget )
		{	Success();
		}
		else if( score >= m_currentTarget - m_currentRange && score <= m_currentTarget + m_currentRange )
		{	Success();
		}
		else
		{	Fail ();
		}


	}

	void ShowSkip()
	{
		if (!ZAdsMgr.Instance.HasRewardedVideoAd ())
			return;

		m_objectSkip.SetActive (true);
	}

	void SpawnCake()
	{
		//if (m_eState == GAME_STATE.SPAWN)
		//	return;

		m_currentCake = ZObjectMgr.Instance.Spawn2D (m_prefabCake.name, new Vector2 (0, m_objectCamera.transform.position.y + 20));
		m_currentCake.transform.localScale = new Vector3 (0.8f, 1f, 1f);
		m_currentCake.GetComponent<Rigidbody2D> ().gravityScale = 0;
		LeanTween.move (m_currentCake, new Vector2 (0, m_objectCamera.transform.position.y + 10), 0.15f);
		m_currentCake.GetComponent<Cake> ().m_textNumber.text = "" + m_currentScore;

		//m_eState = GAME_STATE.SPAWN;
	}

	public void DidCompleteRewardedVideo() {
		Success();
	}

	public override void OnButtonUp(ZButton button)
	{
		if (button.name == "btn_play") {
			ZGameMgr.instance.ShowScene ("ResultsScene");
		}
		else if (button.name == "btn_game") {
			//DropCake();
			if( !m_isSuccess && m_eState == AEGAME_STATE.RESULTS )
				SetupLevel();
			else
				StopGraph();
		}
		else if (button.name == "btn_pause") {
			ZGameMgr.instance.ShowScene ("PauseScene");
		}
		else if (button.name == "btn_showad") {
			ZAdsMgr.Instance.ShowRewardedVideoAds();
		}
		else if (button.name == "btn_showadvideo") {
			//Chartboost.showInterstitial(CBLocation.LevelComplete);
		}


	}
	
	public override void OnButtonDown(ZButton button)
	{
		if (button.name == "btn_game") {
			//ExpandCake();
			ExpandGraph();
		}
	}
}
