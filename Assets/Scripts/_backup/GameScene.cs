﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

//using ChartboostSDK;
using UnityEngine.Advertisements;

public enum GAME_STATE
{
	START,
	IDLE,
	AIMING,
	SHOOT,
	RESULTS
}

public enum GAME_MODES
{
	CLASSIC,
	QUICK,
	ENDLESS,
	ENDLESS_QUICK
}

[System.Serializable]
public class ShopBird
{
	public GameObject objectImage;
	public Image imageButtons;
	public GameObject objectPrice;
	public Sprite spriteImage;
	public RuntimeAnimatorController animator;
	public string m_textName;
}

// iTunes - https://itunes.apple.com/us/app/make-pana-blue-eagle/id1119208392?mt=8
// https://play.google.com/store/apps/details?id=com.mostplayed.archereagle
// itms-apps://itunes.apple.com/app/idYOUR_APP_ID
// from iTunes PH : https://itunes.apple.com/ph/app/make-pana-blue-eagle/id1119208392?mt=8
// itms - itms-apps://itunes.apple.com/ph/app/make-pana-blue-eagle/id1119208392?mt=8

public class GameScene : MonoBehaviour 
{
	public static GameScene instance;

	public GameObject m_rootSpawnLocationLeft;
	public GameObject m_rootSpawnLocationRight;

	public List<ShopBird> m_listShopBirds;
	public List<ShopBird> m_listShopBalls;

	public GameObject m_objectWatchVideo;


	//public GameObject m_objectCharHead;
	public GameObject m_objectParticle;
	public GameObject m_objectParticleBlood;
	public GameObject m_objectParticlePerfect;

	public GameObject m_prefabEnemy;

	public GameObject m_prefabArrow;
	public GameObject m_prefabBird;
	public GameObject m_prefabBirdBig;
	public GameObject m_prefabBall;

	public GameObject m_objectTarget;
	public GameObject m_objectMiss;
	public GameObject m_objectSuccess;
	public GameObject m_objectTutorial;
	public GameObject m_objectTopBar;
	public GameObject m_objectHitParticle;
	public GameObject m_objectHitBow;
	public GameObject m_objectEarned;

	public GameObject m_objectRootArrows;
	public GameObject m_objectArrowMock;
	//public GameObject m_objectTulong;
	public GameObject m_objectNoInternet;
	public GameObject m_objectReadyUnlock;

	public GameObject m_objectTrumpsHairHead;
	public GameObject m_objectTrumpsHairBird;

	public GameObject m_objectHeadshot;

	public GameObject m_objectNormal;
	public GameObject m_objectWin;

	public GameObject m_objectSplash;

	public List<GameObject> m_listButtons;

	public AudioClip m_audioShoot;
	public AudioClip m_audioDie;
	public AudioClip m_audioMiss;
	public AudioClip m_audioMiss1;
	public AudioClip m_audioMiss2;
	public AudioClip m_audioMiss3;
	public AudioClip m_audioMiss4;
	public AudioClip m_audioMiss5;
	public AudioClip m_audioSuccess;
	public AudioClip m_audioBow;
	public AudioClip m_audioButton;
	public AudioClip m_audioHeadshot;
	public AudioClip m_audioCatch;
	public AudioClip m_audioBuy;

	public AudioClip m_audioThrow;

	public Color m_colorTextCorrect;
	public Color m_colorTextMiss;
	public Color m_colorTextSuccess;

	public Text m_textScore;
	public Text m_textLevel;
	public Text m_textHit;
	public Text m_textHit2;
	public Text m_textMiss;
	public Text m_textMessage;
	public Text m_textModes;
	public Text m_textModes2;

	public List<int> m_listPrices;

	public List<string> m_listModes;

	public GameObject m_objectBall;
	public GameObject m_objectBallRoot;

	public GAME_STATE m_eState;
	GameObject m_currentEnemy;

	public List<GameObject> m_listEnemies;

	public List<GameObject> m_listLeftObjects;
	public List<GameObject> m_listRightObjects;

	public GameObject m_objectTutorialFinger;

	int m_isWatchVideo;

	Vector3 m_startPosition;
	Vector2 m_positionTouch;

	int m_currentHighscore;
	int m_currentLevel;
	int m_currentScore;
	int m_currentAds;
	int m_currentHits;
	int m_currentMode;
	int m_currentBird;
	int m_currentPurchaseCount;
	int m_currentAttempts;

	float m_previousPosition = 0;

	float m_currentIncentifiedAdsTime;
	float m_currentDirectionTime;

	float m_currentLeftGroupPosition = -999;
	float m_currentRightGroupPosition = -999;

	bool m_isSuccess = false;

	string m_stringSuffix = " Trumplets";

	bool m_animateIn = true;

	void Awake()
	{
		instance = this;

		Application.targetFrameRate = 60;

		if (!PlayerPrefs.HasKey ("Hit")) {
			PlayerPrefs.SetInt ("Hit", 0);
		}
		if (!PlayerPrefs.HasKey ("Mode")) {
			PlayerPrefs.SetInt ("Mode", 0);
		}
		if (!PlayerPrefs.HasKey ("Removeads")) {
			PlayerPrefs.SetInt ("Removeads", 0);
		}

		if (!PlayerPrefs.HasKey ("currentBird")) {
			PlayerPrefs.SetInt ("currentBird", 0);
		}

		if (!PlayerPrefs.HasKey ("currentPurchaseCount")) {
			PlayerPrefs.SetInt ("currentPurchaseCount", 0);
		}

		if (!PlayerPrefs.HasKey ("attempts")) {
			PlayerPrefs.SetInt ("attempts", 0);
		}

		for (int x = 0; x < 20; x++) {
			if (x == 0) {
				if (!PlayerPrefs.HasKey ("birdbought" + x)) {
					PlayerPrefs.SetInt ("birdbought" + x, 1);
				}
			} else {
				if (!PlayerPrefs.HasKey ("birdbought" + x)) {
					PlayerPrefs.SetInt ("birdbought" + x, 0);
				}
			}
		}

		for (int x = 0; x < 10; x++) {
			if (x == 0) {
				if (!PlayerPrefs.HasKey ("ballbought" + x)) {
					PlayerPrefs.SetInt ("ballbought" + x, 1);
				}
			} else {
				if (!PlayerPrefs.HasKey ("ballbought" + x)) {
					PlayerPrefs.SetInt ("ballbought" + x, 0);
				}
			}
		}

		PlayerPrefs.Save ();

		ZAudioMgr.Instance.enabled = true;
		ZAdsMgr.Instance.enabled = true;
		ZIAPMgr.Instance.enabled = true;
		ZPlatformCenterMgr.Instance.enabled = true;
		ZNotificationMgr.Instance.enabled = true;
	}

	void Start()
	{
		if (!PlayerPrefs.HasKey ("Level")) {
			PlayerPrefs.SetInt ("Level", 1);
		}
		if (!PlayerPrefs.HasKey ("LevelQuick")) {
			PlayerPrefs.SetInt ("LevelQuick", 1);
		}
		if (!PlayerPrefs.HasKey ("LevelPupu")) {
			PlayerPrefs.SetInt ("LevelPupu", 1);
		}
		if (!PlayerPrefs.HasKey ("LevelEndless")) {
			PlayerPrefs.SetInt ("LevelEndless", 0);
		}
		if (!PlayerPrefs.HasKey ("LevelCurve")) {
			PlayerPrefs.SetInt ("LevelCurve", 1);
		}

		if (PlayerPrefs.GetInt ("Removeads") <= 0) {
			ZAdsMgr.Instance.ShowBanner (BANNER_TYPE.BOTTOM_BANNER);
		}

		ZPlatformCenterMgr.Instance.Login ();

		ZObjectMgr.Instance.AddNewObject (m_prefabEnemy.name, 20, "");
		ZObjectMgr.Instance.AddNewObject (m_prefabBall.name, 20, "");

		m_currentPurchaseCount = PlayerPrefs.GetInt ("currentPurchaseCount");
		m_currentHits = PlayerPrefs.GetInt ("Hit");
		m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
		m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
		m_currentBird = PlayerPrefs.GetInt ("currentBird");

		Color splashColor = m_objectSplash.GetComponent<SpriteRenderer> ().color;
		m_objectSplash.GetComponent<SpriteRenderer> ().color = new Color (splashColor.r, splashColor.g, splashColor.b, 1);
		LeanTween.alpha (m_objectSplash, 0, 0.2f);

		//m_objectTulong.SetActive (false);
		m_currentMode = PlayerPrefs.GetInt ("Mode");
		m_currentAttempts = PlayerPrefs.GetInt ("Attempts");
		//MTCharacter.instance.m_currentMode = m_currentMode;
		//m_currentLevel = 12;

		m_currentIncentifiedAdsTime = 60;
		m_currentDirectionTime = 0.1f;

		m_textModes.text = ZGameMgr.instance.m_listLeaderboard[m_currentMode].name + " MODE";
		m_textModes2.text = ZGameMgr.instance.m_listLeaderboard[m_currentMode].name + " MODE";

		m_objectWatchVideo.SetActive (false);
		LoadData ();
		SetupLevel ();
		m_currentAds = 2;
		m_isWatchVideo = 0;

		ZAdsMgr.Instance.RequestInterstitial ();
	}

	Vector3 currentPosition;
	Vector3 previousPosition;
	Vector3 previousMousePosition;
	int m_currentSpawnCount;

	void SpawnEnemyMain()
	{
		if (m_currentSpawnCount <= 0) {
			if (m_currentLevel > 18) {
				int randomSpawnPlackard = Random.Range (0, 100);
				if (randomSpawnPlackard > 95) {
					SpawnEnemy (1, true, false);
					SpawnEnemy (1, true, false);
					SpawnEnemy (-1, true, true);
				} else if (randomSpawnPlackard > 90) {
					SpawnEnemy (1, true, false);
					SpawnEnemy (1, true, true);
					SpawnEnemy (-1, true, false);
				} else if (randomSpawnPlackard > 85) {
					SpawnEnemy (1, true, true);
					SpawnEnemy (1, true, false);
					SpawnEnemy (-1, true, false);
				} else if (randomSpawnPlackard > 80) {
					SpawnEnemy (1, true);
					SpawnEnemy (-1, true, true);
				}  else if (randomSpawnPlackard > 75) {
					SpawnEnemy (1, true, true);
					SpawnEnemy (-1, true);
				} else if (randomSpawnPlackard > 70) {
					SpawnEnemy (1, true);
					SpawnEnemy (-1, true);
				} else {
					SpawnEnemy ();
				}
			}
			else if (m_currentLevel > 13) {
				int randomSpawnPlackard = Random.Range (0, 100);
				if (randomSpawnPlackard > 90) {
					SpawnEnemy (1, true);
					SpawnEnemy (-1, true, true);
				}  else if (randomSpawnPlackard > 80) {
					SpawnEnemy (1, true, true);
					SpawnEnemy (-1, true);
				} else if (randomSpawnPlackard > 70) {
					SpawnEnemy (1, true);
					SpawnEnemy (-1, true);
				} else {
					SpawnEnemy ();
				}
			}
			else if (m_currentLevel > 8) {
				int randomSpawnPlackard = Random.Range (0, 100);
				if (randomSpawnPlackard > 70) {
					SpawnEnemy (1, true);
					SpawnEnemy (-1, true);
				} else {
					SpawnEnemy ();
				}
			} else {
				SpawnEnemy ();
			}
		}
	}

	void Update()
	{
		if (ZGameMgr.instance.isPause)
			return;
		
		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (m_objectShop.activeSelf) {
				m_objectShop.SetActive (false);
				ZAudioMgr.Instance.PlaySFX (m_audioButton);
			}
			else
				Application.Quit ();
		}


		//if (m_currentMode == 0) {
			if (m_eState != GAME_STATE.RESULTS && !m_isSuccess) {
			/*if (m_currentDirectionTime <= 0) {Update(
				if (m_previousPosition > MTCharacter.instance.transform.position.x) {
					MTCharacter.instance.SetDirection (-1);
				} else if (m_previousPosition < MTCharacter.instance.transform.position.x) {
					MTCharacter.instance.SetDirection (1);
				}
				m_currentDirectionTime = 0.1f;
				m_previousPosition = MTCharacter.instance.transform.position.x;
			} else {
				m_currentDirectionTime -= Time.deltaTime;
			}*/

				switch (m_eState) {
				case GAME_STATE.START:
				if (Input.GetMouseButtonUp (0)) {
					SpawnEnemyMain ();
					m_eState = GAME_STATE.IDLE;
					//m_textLevel.text = "";
					m_textScore.color = m_colorTextCorrect;
					//m_objectTulong.SetActive (false);Update(
					//m_textHit.gameObject.SetActive (false);
					Play ();
					m_objectArrowMock.SetActive (true);
				}


					break;
				case GAME_STATE.IDLE:
					/*m_objectBall.GetComponent<Rigidbody2D> ().isKinematic = true;
					if (Input.GetMouseButtonDown (0))
					{
						m_eState = GAME_STATE.AIMING;
						previousMousePosition = Input.mousePosition;
					}
					break;*/

					if (Input.GetMouseButtonDown (0) || Input.GetMouseButton (0)) {
						//LeanTween.cancel (m_objectCharHead);
						//m_startPosition = Input.mousePosition;
						m_eState = GAME_STATE.AIMING;
						ZAudioMgr.Instance.PlaySFX (m_audioBow);
						m_objectArrowMock.SetActive (true);
						m_objectTutorial.SetActive (false);
						//m_objectTutorialFinger.SetActive (false);

						if (m_objectTutorialFinger.GetComponent<SpriteRenderer> ().color.a >= 0.3f) {
							LeanTween.alpha (m_objectTutorialFinger, 0, 0.2f);
						}

						Vector3 mousePosition = Input.mousePosition;
						Vector3 charPosition = MTCharacter.instance.transform.position;
						Vector3 newPosition = Camera.main.ScreenToWorldPoint (mousePosition);
						MTCharacter.instance.transform.position = new Vector3 (newPosition.x, charPosition.y, charPosition.z);

						float defaultScale = MTCharacter.instance.m_defaultScale;
						LeanTween.scale (MTCharacter.instance.gameObject, new Vector3(defaultScale+0.2f, defaultScale+0.2f,defaultScale+0.2f ), 0.2f);

						LeanTween.scale (m_objectTrumpsHairHead, new Vector3 (1, 0, 1), 0.1f);
						LeanTween.scale (m_objectTrumpsHairBird, new Vector3 (0.7f, 0.7f, 0.7f), 0.1f);
						//m_objectTrumpsHairBird.SetActive (true);
					}


					break;
				case GAME_STATE.AIMING:

					/*if (Input.GetMouseButtonUp (0)) 
					{
						currentPosition = Input.mousePosition;

						//previousPosition = m_objectBallRoot.transform.position;//Camera.main.ScreenToWorldPoint (previousPosition);
						previousPosition = Camera.main.ScreenToWorldPoint (previousMousePosition);
						currentPosition = Camera.main.ScreenToWorldPoint (currentPosition);

						float angle = Mathf.Atan2 (previousPosition.y - currentPosition.y, previousPosition.x - currentPosition.x);
						Vector3 direction = Quaternion.AngleAxis(angle * 180 / Mathf.PI, Vector3.forward) * Vector3.left * 0.5f;	

						Vector2 newDirection = direction * 4500f;

						if (Vector3.Distance (previousMousePosition, Input.mousePosition) > 20f) {	//newDirection = Vector3.zero;
						
							m_objectBall.GetComponent<Rigidbody2D> ().isKinematic = false;
							m_objectBall.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (newDirection.x * 0.2f, 2000f));//new Vector2(0, 2000f));
							m_objectBall.GetComponent<Rigidbody2D> ().AddTorque (Random.Range (-550f, 550));
							LeanTween.scale (m_objectBall, new Vector3 (0.4f, 0.4f, 0.4f), 1f);//.setEase(LeanTweenType.easeInCubic);

							ZAudioMgr.Instance.PlaySFX (m_audioThrow);

							m_eState = GAME_STATE.SHOOT;
						}
						//m_eState = GAME_STATE.IDLE;
					}
					break;*/

				Vector3 mouseDelta = Input.mousePosition - m_startPosition;
					m_objectArrowMock.SetActive (true);
				if (mouseDelta.sqrMagnitude < 0.1f) {
					return; // don't do tiny rotations.
				}

					m_objectTarget.SetActive (true);

					Vector3 mousePosition2 = Input.mousePosition;
					Vector3 charPosition2 = MTCharacter.instance.transform.position;
					Vector3 newPosition2 = Camera.main.ScreenToWorldPoint (mousePosition2);
					MTCharacter.instance.transform.position = new Vector3 (newPosition2.x, charPosition2.y, charPosition2.z);

				float angle = Mathf.Atan2 (Input.mousePosition.y, Input.mousePosition.x) * Mathf.Rad2Deg;
				if (angle < 0)
					angle += 360;

				//Debug.Log (angle);

				//m_objectCharHead.transform.localEulerAngles = new Vector3 (transform.localEulerAngles.x,
					//transform.localEulerAngles.y, 
					//angle + 30);


					if (Input.GetMouseButtonUp (0)) {
						////LeanTween.rotateZ(m_objectCharHead, 0, 0.5f);
						//ShootArrow ();
						MTCharacter.instance.Poop ();
						//m_objectTarget.SetActive (false);
						m_objectArrowMock.SetActive (false);
						m_eState = GAME_STATE.SHOOT;
						float defaultScale = MTCharacter.instance.m_defaultScale;
						LeanTween.scale (MTCharacter.instance.gameObject, new Vector3(defaultScale, defaultScale,defaultScale ), 0.2f);

					LeanTween.cancel (m_objectTrumpsHairBird);
						m_objectTrumpsHairBird.transform.localScale = Vector3.zero;
					}
					break;
				}
			}


		/*} else {
			if (m_eState != GAME_STATE.START && Input.GetMouseButtonDown (0)) {
				//LeanTween.cancel (m_objectCharHead);
				//m_startPosition = Input.mousePosition;
				//m_eState = GAME_STATE.AIMING;
				if (m_currentMode == 3) {
					MTCharacter.instance.Poop ();
				} else if (m_eState != GAME_STATE.RESULTS) {
					MTCharacter.instance.Swoop ();
				}
			}
		}*/



		/*switch (m_eState) {
		case GAME_STATE.START:
			//if (Input.GetMouseButtonUp (0)) {
				//SpawnEnemy ();
				//m_eState = GAME_STATE.IDLE;
				/*m_textLevel.text = "";
				m_textScore.color = m_colorTextCorrect;
				m_objectTulong.SetActive (false);
				m_textHit.gameObject.SetActive (false);*/
			//}
			//m_objectArrowMock.SetActive (true);
		/*
			break;
		case GAME_STATE.IDLE:
			if (Input.GetMouseButtonDown (0)) {
				//LeanTween.cancel (m_objectCharHead);
				m_startPosition = Input.mousePosition;
				m_eState = GAME_STATE.AIMING;
				MTCharacter.instance.Swoop ();
				//ZAudioMgr.Instance.PlaySFX (m_audioBow);
				//m_objectArrowMock.SetActive (true);
			}


			break;
		case GAME_STATE.AIMING:
			/*Vector3 mouseDelta = Input.mousePosition - m_startPosition;
			m_objectArrowMock.SetActive (true);
			if (mouseDelta.sqrMagnitude < 0.1f) {
				return; // don't do tiny rotations.
			}

			m_objectTarget.SetActive (true);


			float angle = Mathf.Atan2 (Input.mousePosition.y, Input.mousePosition.x) * Mathf.Rad2Deg;
			if (angle < 0)
				angle += 360;

			//Debug.Log (angle);

			m_objectCharHead.transform.localEulerAngles = new Vector3 (transform.localEulerAngles.x,
				transform.localEulerAngles.y, 
				angle + 30);
			if (Input.GetMouseButtonUp (0)) {
				//LeanTween.rotateZ(m_objectCharHead, 0, 0.5f);
				ShootArrow ();
				m_objectTarget.SetActive (false);
				m_objectArrowMock.SetActive (false);
				m_eState = GAME_STATE.SHOOT;
			}*/
			//break;
		//}

		/*switch (m_eState) {
		/*case GAME_STATE.START:
			/*if (Input.GetMouseButtonDown (0)) {
				SpawnEnemy ();
				m_eState = GAME_STATE.IDLE;
				m_textLevel.text = "";
				m_textScore.color = m_colorTextCorrect;
				m_objectTulong.SetActive (false);
				m_textHit.gameObject.SetActive (false);
			}*/
			/*m_objectArrowMock.SetActive (true);

			break;
		case GAME_STATE.IDLE:
			if (Input.GetMouseButtonDown (0) || Input.GetMouseButton (0)) {
				LeanTween.cancel (m_objectCharHead);
				m_startPosition = Input.mousePosition;
				m_eState = GAME_STATE.AIMING;
				ZAudioMgr.Instance.PlaySFX (m_audioBow);
				m_objectArrowMock.SetActive (true);
			}


			break;
		case GAME_STATE.AIMING:
			Vector3 mouseDelta = Input.mousePosition - m_startPosition;
			m_objectArrowMock.SetActive (true);
			if (mouseDelta.sqrMagnitude < 0.1f) {
				return; // don't do tiny rotations.
			}

			m_objectTarget.SetActive (true);


			float angle = Mathf.Atan2 (Input.mousePosition.y, Input.mousePosition.x) * Mathf.Rad2Deg;
			if (angle < 0)
				angle += 360;

			//Debug.Log (angle);
			
			m_objectCharHead.transform.localEulerAngles = new Vector3 (transform.localEulerAngles.x,
				transform.localEulerAngles.y, 
				angle + 30);
			if (Input.GetMouseButtonUp (0)) {
				//LeanTween.rotateZ(m_objectCharHead, 0, 0.5f);
				ShootArrow ();
				m_objectTarget.SetActive (false);
				m_objectArrowMock.SetActive (false);
				m_eState = GAME_STATE.SHOOT;
			}
			break;
		}*/

		m_currentIncentifiedAdsTime -= Time.deltaTime;

	}

	void LoadData()
	{

	}

	public void Hit(Vector3 position)
	{
		//if (m_eState == GAME_STATE.RESULTS)
		//	return;

		m_objectHitParticle.GetComponent<ParticleSystem> ().Clear ();
		m_objectHitParticle.transform.position = position;
		m_objectHitParticle.SetActive (true);
		m_objectHitParticle.GetComponent<ParticleSystem> ().Play ();

		ZAudioMgr.Instance.PlaySFX (m_audioCatch);
		m_currentSpawnCount--;
		//m_eState = GAME_STATE.IDLE;

		//m_currentHits++;
	}

	public void HitHeadshot(Vector3 position)
	{
	m_objectParticlePerfect.GetComponent<ParticleSystem> ().Clear ();
	m_objectParticlePerfect.transform.position = position;
	m_objectParticlePerfect.SetActive (true);
	m_objectParticlePerfect.GetComponent<ParticleSystem> ().Play ();

		m_objectHeadshot.SetActive (true);
		LeanTween.scale (m_objectHeadshot, new Vector3(1.2f, 1.2f, 1.2f), 0.4f).setEase(LeanTweenType.easeInOutCubic).setLoopCount(2).setLoopPingPong().setOnComplete(HitHeadshotRoutine);
		//LeanTween.scale (m_textScore.gameObject, new Vector3(1.2f, 1.2f, 1.2f), 0.4f).setEase(LeanTweenType.easeInOutCubic).setLoopCount(2).setLoopPingPong().setOnComplete(HitHeadshotRoutine);
		//Score ();

		ZAudioMgr.Instance.PlaySFX (m_audioHeadshot);
		m_currentSpawnCount--;
		//Debug.LogError ("Headshot");
	}

	void HitHeadshotRoutine()
	{
		m_objectHeadshot.SetActive (false);
	}

	int rewardedSource = 0;
	public void Die()
	{
		if (m_eState == GAME_STATE.RESULTS)
			return;
		//StopCoroutine ("whileTest");
		m_textScore.color = m_colorTextMiss;
		m_objectMiss.SetActive (true);

		StopCoroutine ("whileTest");
		ZAnalytics.Instance.SendLevelFail ("mode" + m_currentMode, m_currentLevel, m_currentScore);

		int chatvalue = Random.Range (0, 9);
		switch (chatvalue) {
		case 0 : 
			m_textMiss.text = "Make Hair Real Again!";
			break;
		case 1 : 
			m_textMiss.text = "We Shall Never Comb!";
			break;
		case 2 : 
			m_textMiss.text = "Toupee!";
			break;
		case 3 : 
			m_textMiss.text = "We Shall Overcomb!";
			break;
		case 4 : 
			m_textMiss.text = "Up the Hill!";
			break;
		case 5 : 
			m_textMiss.text = "My Royal Hair!";
			break;
		case 6 : 
			m_textMiss.text = "Make America Migrate Again!";
			break;
		case 7 : 
			m_textMiss.text = "You're Fired!";
			break;
		default : 
			m_textMiss.text = "Hair Dump!";
			break;
		}

	LeanTween.cancel (m_textMiss.gameObject);
	LeanTween.scale (m_textMiss.gameObject, m_textMiss.gameObject.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);

		//MTCharacter.instance.Die ();
		ZCameraMgr.instance.DoShake ();

		ZAudioMgr.Instance.PlaySFX (m_audioDie);
		//m_textScore.text = "";
		//Debug.LogError ("Miss");

	/*
		if (Advertisement.IsReady ("rewardedVideo") &&
			m_currentScore < m_currentLevel/2 && 
			m_currentIncentifiedAdsTime <= 0 && 
			m_currentLevel > 50 && false) 
		{
			rewardedSource = 0;
			ShowRewardedAdPopup ("One more chance?", "Watch an ad to continue!");
			m_currentIncentifiedAdsTime = 180;
		} else if (Advertisement.IsReady ("rewardedVideo") && m_currentIncentifiedAdsTime <= 0 && m_currentLevel > 3) {
			//ShowAdPopup ("Make Support the Dev!","Click yes to watch an ad!");
			//m_currentIncentifiedAdsTime = 180;

			LeanTween.delayedCall (2f, SetupLevel);
			rewardedSource = 1;
			//ShowRewardedAdPopup ("Get 100 Eagles!", "Click yes to watch an ad!");
			//m_objectWatchVideo.SetActive(true);
			m_isWatchVideo = 2;
			m_currentIncentifiedAdsTime = 180;
		}else {
			LeanTween.delayedCall (2f, SetupLevel);

			m_currentAds++;
			if (m_currentLevel > 4 && 
				m_currentAds > 4 && 
				ZAdsMgr.Instance.removeAds <= 0 ) 
			{
				if (Advertisement.IsReady("video")) 
				{
					Advertisement.Show ("video");
					m_currentAds = 0;
					//m_objectTulong.SetActive (true);
				}
			}
		}*/

		/*m_currentAds++;
		if (m_currentLevel > 5 && 
			m_currentAds > 10 && 
			ZAdsMgr.Instance.removeAds <= 0 ) 
		{
			if (Advertisement.IsReady("video")) 
			{
				Advertisement.Show ("video");
				m_currentAds = 0;
			}
		}*/

		int randomMiss = Random.Range (0, 100);
		if( randomMiss > 80 )
			ZAudioMgr.Instance.PlaySFX (m_audioMiss);
		else if(  randomMiss > 70 )
			ZAudioMgr.Instance.PlaySFX (m_audioMiss1);
		else if(  randomMiss > 60 )
			ZAudioMgr.Instance.PlaySFX (m_audioMiss2);
		else if(  randomMiss > 50 )
			ZAudioMgr.Instance.PlaySFX (m_audioMiss3);
		else if(  randomMiss > 30 )
			ZAudioMgr.Instance.PlaySFX (m_audioMiss4);
		else
			ZAudioMgr.Instance.PlaySFX (m_audioMiss5);
	
		m_eState = GAME_STATE.RESULTS;

		//m_currentEnemy.GetComponent<Rigidbody2D> ().velocity = new Vector2 (-10f, 0);
		/*foreach (GameObject obj in m_listBirds) {
			obj.GetComponent<Rigidbody2D> ().velocity = new Vector2 (-10f, 0);
		}*/

		foreach (GameObject obj2 in m_listEnemies) {
			if (obj2 && m_objectBall && obj2.transform.position.x > m_objectBall.transform.position.x) {
				//m_currentEnemy.GetComponent<MTEnemy> ().RunFast ();
				//m_currentEnemy.GetComponent<Rigidbody2D> ().velocity = new Vector2 (10f, 0);
				//float enemyScale = m_currentEnemy.GetComponent<MTEnemy> ().m_currentScale;
				//m_currentEnemy.transform.localScale = new Vector3 (-enemyScale, enemyScale, enemyScale);
				if (obj2.GetComponent<MTEnemy> ().m_currentState != MTENEMY_STATES.DIE)
					obj2.GetComponent<MTEnemy> ().Reset (-1, 5f, 0);
				//m_currentEnemy.GetComponent<MTEnemy> ().MoveRight ();
			} else if (obj2 && m_objectBall) {
				//m_currentEnemy.GetComponent<MTEnemy> ().MoveLeft ();
				if (obj2.GetComponent<MTEnemy> ().m_currentState != MTENEMY_STATES.DIE)
					obj2.GetComponent<MTEnemy> ().Reset (1, 5f, 0);
				//m_currentEnemy.GetComponent<Rigidbody2D> ().velocity = new Vector2 (-10f, 0);
				//float enemyScale = m_currentEnemy.GetComponent<MTEnemy> ().m_currentScale;
				//m_currentEnemy.transform.localScale = new Vector3 (enemyScale, enemyScale, enemyScale);
			}
		}
		m_listEnemies.Clear ();
	
		PlayerPrefs.SetInt ("Hit", m_currentHits);
	m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
	m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;

		if (m_currentMode == 1 ){
			if (m_currentScore > PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName)) {
				PlayerPrefs.SetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName, m_currentScore);
				m_currentLevel = m_currentScore;
			}
		}

		ZAnalytics.Instance.SendEarnCoinsEvent ("mode" + m_currentMode, m_currentLevel - m_currentScore);
		//ZAnalytics.Instance.SendScore (m_currentScore);

		/*if (m_currentHits >= GetCurrentShopPrice ()) {
			ZRewardsMgr.instance.ShowShopButton ();
		}*/
		if (Advertisement.IsReady ("rewardedVideo") &&
		    m_currentScore < m_currentLevel / 2 &&
		    m_currentIncentifiedAdsTime <= 0 && 
			m_currentMode == 0 && 
			m_currentLevel > 5 ) {
			LeanTween.delayedCall (1f, ShowContinueRoutine);
		} /*else if (Advertisement.IsReady ("rewardedVideo") &&
			m_currentScore > m_currentLevel / 2 &&
			m_currentIncentifiedAdsTime <= 0 && 
			m_currentMode == 1 && 
			m_currentLevel > 5 ) {
			LeanTween.delayedCall (1f, ShowContinueRoutine);
		}*/
		else if (m_currentLevel > 2 && m_currentAds > 2 && ZAdsMgr.Instance.IsInterstitialAvailable()) {
			ZAdsMgr.Instance.ShowInsterstitial ();
			m_currentAds = 0;
			LeanTween.delayedCall (1.75f, SetupLevel);
		}
		else if (m_currentHits >= GetCurrentShopPrice () && 
			m_currentPurchaseCount < m_listShopBirds.Count-1 && 
			ZRewardsMgr.instance.WillShowShopButton()) {
			ZRewardsMgr.instance.ShowShopButton ();
			
			LeanTween.delayedCall (1f, ShowShopAvailable);
		}
		else if (m_currentLevel > 2 && ZRewardsMgr.instance.WillShowReward ()) {
			LeanTween.delayedCall (1f, ShowReward);
		} else {
			LeanTween.delayedCall (2.25f, SetupLevel);
		}
		
		m_currentAds++;

		#if UNITY_ANDROID
		if (m_currentMode == 1) {
		ZPlatformCenterMgr.Instance.PostScore (PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[1].saveName), ZGameMgr.instance.m_listLeaderboard[1].leaderboardAndroid);
		}
		#else
		if (m_currentMode == 1) {
	ZPlatformCenterMgr.Instance.PostScore (PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[1].saveName), ZGameMgr.instance.m_listLeaderboard[1].leaderboardIOS);
		}
		#endif // UNITY_ANDROID

		m_currentAttempts++;
		PlayerPrefs.SetInt ("attempts", m_currentAttempts);

		/*m_currentAds++;
		if (m_currentLevel > 4 && m_currentAds > 5) 
		{
			if (Advertisement.IsReady("video")) 
			{
				Advertisement.Show ("video");
				m_currentAds = 0;
				m_objectTulong.SetActive (true);
			}

			m_currentAds = 0;
		}*/
	}

	void ShowContinueRoutine(){
		ShowRewardedAdPopup ("Continue?", "Watch an ad to continue!");
		m_currentIncentifiedAdsTime = 120;
	}

	void ShowShopAvailable(){
		m_objectReadyUnlock.SetActive (true);
		LeanTween.scale (m_objectReadyUnlock, m_objectReadyUnlock.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);
		LeanTween.delayedCall (1.505f, SetupLevel);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	void ShowReward(){
		LeanTween.delayedCall (2f, SetupLevel);
		ZRewardsMgr.instance.ShowReward ();
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	void DieForce()
	{
	}

	public void Score(int score)
	{
		if (m_currentMode == 1) {
			m_currentScore += score;
			m_textScore.text = "" + m_currentScore;
			m_currentLevel = m_currentScore + Mathf.FloorToInt((m_currentHighscore/2f));
			SpawnEnemyMain ();
			m_eState = GAME_STATE.IDLE;
			//LeanTween.delayedCall (0.01f, ScoreRoutine);

		} else {
			if (m_currentScore <= 0)
				return;
		
			m_currentScore -= score;
			if (m_currentScore < 0)
				m_currentScore = 0;
		
			m_textScore.text = "" + m_currentScore;



			if (m_currentScore <= 0) {
				Success ();
			} else if (m_currentMode == 1) {
			m_eState = GAME_STATE.IDLE;
			} else {
				//LeanTween.delayedCall (0.5f, ScoreRoutine);
				SpawnEnemyMain ();
			m_eState = GAME_STATE.IDLE;
			}
		}

		/*if (m_currentMode == 3) {
			m_eState = GAME_STATE.IDLE;
		}*/

		AddCoins (score);

		//SpawnBall ();
	//m_objectTrumpsHairHead.SetActive (true);
	LeanTween.scale (m_objectTrumpsHairHead, new Vector3 (1, 1, 1), 0.1f);
		
	/*
		// TUNING
		if (m_currentLevel > 50) {
			MTCharacter.instance.SetMaxSpeed (2f);
			MTCharacter.instance.SetSpawnRange (-2f, 10f);
		} else if (m_currentLevel > 25) {
			MTCharacter.instance.SetMaxSpeed (1.9f);
			MTCharacter.instance.SetSpawnRange (-2f, 10f);
		} else if (m_currentLevel > 10) {
			MTCharacter.instance.SetMaxSpeed (1.75f);
			MTCharacter.instance.SetSpawnRange (-2f, 9f);
		} else if (m_currentLevel > 5) {
			MTCharacter.instance.SetMaxSpeed (1.6f);
			MTCharacter.instance.SetSpawnRange (-2f, 8f);
		} else {
			MTCharacter.instance.SetMaxSpeed (1.5f);
			MTCharacter.instance.SetSpawnRange (-2f, 7f);
		}*/
	}

	void Success()
	{
		if (m_eState == GAME_STATE.RESULTS)
			return;
		//ZAnalytics.Instance.SendScore (m_currentLevel);
		ZAnalytics.Instance.SendEarnCoinsEvent ("mode" + m_currentMode, m_currentLevel);
		ZAnalytics.Instance.SendLevelComplete ("mode" + m_currentMode, m_currentLevel, m_currentAttempts);
		m_textScore.color = m_colorTextSuccess;
		m_textLevel.color = m_colorTextSuccess;
		ZAudioMgr.Instance.PlaySFX (m_audioDie);

		m_objectSuccess.SetActive (true);
	LeanTween.cancel (m_objectSuccess.gameObject);
	LeanTween.scale (m_objectSuccess.gameObject, m_textMiss.gameObject.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);


		m_objectParticle.SetActive (true);
		StopCoroutine ("whileTest");
		LeanTween.delayedCall (3.5f, SuccessRoutine);
		m_eState = GAME_STATE.RESULTS;

		float defaultScale = MTCharacter.instance.m_defaultScale;
		LeanTween.scale (MTCharacter.instance.gameObject, new Vector3(defaultScale+0.4f, defaultScale+0.4f,defaultScale+0.4f ), 0.2f);

		m_objectNormal.SetActive (false);
		m_objectWin.SetActive (true);

		//ZObjectMgr.Instance.ResetAll ();

		m_currentAttempts++;
		PlayerPrefs.SetInt ("attempts", m_currentAttempts);

		//LeanTween.moveLocalY (m_objectHitBow, m_objectHitBow.transform.localPosition.y + 2, 0.5f).setLoopCount (6).setLoopPingPong();

		PlayerPrefs.SetInt ("Hit", m_currentHits);
	m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
	m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;

		ZAudioMgr.Instance.PlaySFX (m_audioSuccess);

		ZAnalytics.Instance.SendCurrentCoins (m_currentHits);
		LeanTween.scale (m_objectTrumpsHairHead, new Vector3 (1, 1, 1), 0.1f);

		foreach (GameObject obj2 in m_listEnemies) {
			if (obj2 && obj2.transform.position.x > m_objectBall.transform.position.x) {
			//m_currentEnemy.GetComponent<MTEnemy> ().RunFast ();
			//m_currentEnemy.GetComponent<Rigidbody2D> ().velocity = new Vector2 (10f, 0);
			//float enemyScale = m_currentEnemy.GetComponent<MTEnemy> ().m_currentScale;
			//m_currentEnemy.transform.localScale = new Vector3 (-enemyScale, enemyScale, enemyScale);
				if (obj2.GetComponent<MTEnemy> ().m_currentState != MTENEMY_STATES.DIE)
					obj2.GetComponent<MTEnemy> ().Reset (-1, 5f, 0);
			//m_currentEnemy.GetComponent<MTEnemy> ().MoveRight ();
			} else if (obj2) {
			//m_currentEnemy.GetComponent<MTEnemy> ().MoveLeft ();
				if (obj2.GetComponent<MTEnemy> ().m_currentState != MTENEMY_STATES.DIE)
					obj2.GetComponent<MTEnemy> ().Reset (1, 5f, 0);
			//m_currentEnemy.GetComponent<Rigidbody2D> ().velocity = new Vector2 (-10f, 0);
			//float enemyScale = m_currentEnemy.GetComponent<MTEnemy> ().m_currentScale;
			//m_currentEnemy.transform.localScale = new Vector3 (enemyScale, enemyScale, enemyScale);
			}
		}
		m_listEnemies.Clear ();

		/*if (ZRateUsMgr.Instance.isRateUs <= 0 && m_currentLevel == 5) {
			ZRateUsMgr.Instance.ShowRateUs();
		}
		else if (ZFollowUsMgr.Instance.isFollowUs <= 0 && m_currentLevel == 10 ) {
			ZFollowUsMgr.Instance.ShowFollowUs();
		}
		/*else if (m_currentIncentifiedAdsTime <= 0 && m_currentLevel > 5) {
			ShowAdPopup ("Make Support the Dev!","Click yes to watch an ad!");
			m_currentIncentifiedAdsTime = 180;
		}*/
		/*else if (Advertisement.IsReady ("rewardedVideo") && m_currentIncentifiedAdsTime <= 0 && m_currentLevel > 3) {
			//ShowAdPopup ("Make Support the Dev!","Click yes to watch an ad!");
			//m_currentIncentifiedAdsTime = 180;

			rewardedSource = 1;
			//ShowRewardedAdPopup ("Get 100 Eagles!", "Click yes to watch an ad!");
			m_currentIncentifiedAdsTime = 180;
			//m_objectWatchVideo.SetActive(true);
			m_isWatchVideo = 2;
		}
		else if (ZRateUsMgr.Instance.isRateUs <= 0 && m_currentLevel % 5 == 0) {
			ZRateUsMgr.Instance.ShowRateUs();
		}
		else if (ZFollowUsMgr.Instance.isFollowUs <= 0 && m_currentLevel % 5 == 0) {
			ZFollowUsMgr.Instance.ShowFollowUs();
		}*/

		if (m_currentMode == 1) {
			foreach (GameObject obj in m_listBirds) {
				obj.GetComponent<Rigidbody2D> ().velocity = new Vector2 (-10f, 0);
			}
		}

		if (m_currentMode == 3) {
			m_isSuccess = true;
			//MTCharacter.instance.Run (4);
			//MTCharacter.instance.RunStraight (4);
			//MTCharacter.instance.Success();
		}

		m_currentAds++;

		#if UNITY_ANDROID
		/*if (m_currentMode == 0) {
			ZPlatformCenterMgr.Instance.PostScore ( PlayerPrefs.GetInt ("Level"), ZGameMgr.instance.LEADERBOARD_ANDROID);
		}
		else if (m_currentMode == 1) {
			ZPlatformCenterMgr.Instance.PostScore (PlayerPrefs.GetInt ("LevelQuick"), "CgkI85r1lMsYEAIQAg");
		}
		else if (m_currentMode == 2) {
			ZPlatformCenterMgr.Instance.PostScore (PlayerPrefs.GetInt ("LevelEndless"), "CgkI85r1lMsYEAIQAw");
		}*/
		ZPlatformCenterMgr.Instance.PostScore(PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName), 
												ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardAndroid);

		#else
		/*if (m_currentMode == 0) {
		ZPlatformCenterMgr.Instance.PostScore ( PlayerPrefs.GetInt ("Level"), ZGameMgr.instance.LEADERBOARD_IOS);
		}
		else if (m_currentMode == 1) {
		ZPlatformCenterMgr.Instance.PostScore (PlayerPrefs.GetInt ("LevelQuick"), "highscore_leaderboard_quick");
		}
		else if (m_currentMode == 2) {
		ZPlatformCenterMgr.Instance.PostScore (PlayerPrefs.GetInt ("LevelEndless"), "highscore_leaderboard_endless");
		}*/
		ZPlatformCenterMgr.Instance.PostScore(PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName), 
												ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardIOS);
		#endif // UNITY_ANDROID

		

		//m_currentAds++;
	}

	public void ShowAdPopup(string title, string message)
	{	
		if (ZAdsMgr.Instance.removeAds > 0)
			return;
		
		//if( isRateUs > 0 )
		//	return;
		if (!Advertisement.IsReady ("rewardedVideo"))
			return;

		MobileNativeDialog ratePopUp = new MobileNativeDialog(title, message);
		ratePopUp.OnComplete += OnShowAdPopUpClose;

		//isRateUs++;
	}
	private void OnShowAdPopUpClose(MNDialogResult result) {
		if (result == MNDialogResult.YES) {
			Advertisement.Show ("rewardedVideo");
			//m_objectTulong.SetActive (true);
			//m_currentAds = 0;
		} else {
			//m_currentAds = 2;
		}
	}

	public void ShowRewardedVideo()
	{
		ShowOptions options = new ShowOptions();
		options.resultCallback = AdCallbackhanler;
		Advertisement.Show ("rewardedVideo", options);

		m_objectWatchVideo.SetActive (false);

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public void ShowRewardedAdPopup(string title, string message)
	{	
		//if (ZAdsMgr.Instance.removeAds > 0)
		//	return;
		//if( isRateUs > 0 )
		//	return;
		//if (!Advertisement.IsReady ("rewardedVideo"))
		//	return;

		MobileNativeDialog ratePopUp = new MobileNativeDialog(title, message);
		ratePopUp.OnComplete += OnShowRewardedAdPopUpClose;

		//isRateUs++;
	}
	private void OnShowRewardedAdPopUpClose(MNDialogResult result) 
	{
		if (result == MNDialogResult.YES) 
		{
			ShowOptions options = new ShowOptions();
			options.resultCallback = AdCallbackhanler;
			Advertisement.Show ("rewardedVideo", options);
		} else 
		{
			LeanTween.delayedCall (1f, SetupLevel);
		}
	}

	void AdCallbackhanler(ShowResult result)
	{
		switch (result)
		{
		case ShowResult.Finished:
			//if (rewardedSource == 0)
				ContinueLevel ();
			//if (rewardedSource == 1) {
			//	AddCoins (100);
			//	m_objectEarned.SetActive (true);
				//new MobileNativeMessage("You get 100 Eagles", "Thank you for your Support!");
				//m_objectTulong.SetActive (true);
			//}
			//m_currentAds = 0;
			break;
		case ShowResult.Skipped:
			LeanTween.delayedCall (1f, SetupLevel);
			break;
		case ShowResult.Failed:
			LeanTween.delayedCall (1f, SetupLevel);
			break;
		}
	}

	void SuccessRoutine()
	{
		//m_objectSuccess.SetActive (false);
		m_textLevel.text = "Level";
		m_textScore.text = "" + m_currentLevel;
		LeanTween.delayedCall (1f, SuccessRoutine2);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	void SuccessRoutine2()
	{
		m_textScore.text = "" + (m_currentLevel+1);
		LeanTween.delayedCall (1.5f, SuccessRoutine3);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		if (m_currentLevel > 1 && ZRewardsMgr.instance.WillShowRewardSuccess ()) {
			//LeanTween.delayedCall (1f, ShowReward);
			ZRewardsMgr.instance.ShowRewardSuccess ();
		}
	}

	void SuccessRoutine3()
	{
		m_objectSuccess.SetActive (false);
		m_currentLevel++;
		ResetScore();
		//PlayerPrefs.SetInt ("Level", m_currentLevel);

		/*if( m_currentMode == 0 )
			PlayerPrefs.SetInt ("Level", m_currentLevel);
		else if( m_currentMode == 1 )
			PlayerPrefs.SetInt ("LevelQuick", m_currentLevel);
		else if( m_currentMode == 2 )
			PlayerPrefs.SetInt ("LevelEndless", m_currentLevel);*/

		PlayerPrefs.SetInt (ZGameMgr.instance.m_listLeaderboard [m_currentMode].saveName, m_currentLevel);

		/*if (m_currentLevel > 2 && ZRewardsMgr.instance.WillShowRewardSuccess ()) {
			//LeanTween.delayedCall (1f, ShowReward);
			ZRewardsMgr.instance.ShowRewardSuccess ();
			LeanTween.delayedCall (1f, SetupLevel);
		} else {*/
			SetupLevel ();
		//}

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	void ScoreRoutine()
	{
		if( m_currentMode != 1)
			SpawnEnemy ();
	}

	void SpawnEnemyQuick()
	{
		if (m_eState == GAME_STATE.RESULTS)
			return;

		StartCoroutine("whileTest");
		SpawnEnemy ();
	}

	public int GetLevel(){
		return m_currentLevel;
	}

	IEnumerator whileTest()
	{
		while(true == true)
		{
		/*
			if(m_currentLevel > 50)
				yield return new WaitForSeconds(Random.Range (5f, 6f));
			else if(m_currentLevel > 25)
				yield return new WaitForSeconds(Random.Range (5f, 6.5f));
			else
				yield return new WaitForSeconds(Random.Range (5f, 7f));
				*/

			SpawnEnemy ();
		//Random.Range (1.4f, 2.5f
			yield return new WaitForSeconds(Random.Range (1.8f, 2.2f));
			Debug.Log ("while test working");
		}

	}

	public void Play()
	{	
		//if( m_currentMode == 0 || m_currentMode == 2)// || m_currentMode == 3 )// || m_currentMode == 3)
		//	SpawnEnemy (0, true);
	//if (m_currentMode == 1)
		//	SpawnEnemy (0, true);
		//	StartCoroutine("whileTest");
	//	SpawnEnemyQuick ();

		if (m_currentMode == 1) {
			m_currentScore = 0;
			m_textScore.text = "" + m_currentScore;
			m_currentHighscore = m_currentLevel;
		}


		//m_objectTutorialFinger.SetActive (true)
		//SpawnEnemy ();

		if (m_objectTutorialFinger.GetComponent<SpriteRenderer> ().color.a == 0) {
			LeanTween.alpha (m_objectTutorialFinger, 1, 0.2f);
		}

		
		//m_objectBall.GetComponent<Rigidbody2D> ().isKinematic = true;
		m_eState = GAME_STATE.IDLE;
		m_textLevel.text = "";
		m_textScore.color = m_colorTextCorrect;
		//m_objectTulong.SetActive (false);
		//m_textHit.gameObject.SetActive (false);

		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		
		m_objectTopBar.SetActive(false);
		//m_objectArrowMock.SetActive (true);
		m_objectEarned.SetActive (false);
		m_objectNoInternet.SetActive (false);

	m_objectTutorial.GetComponent<Text> ().text = "HOLD TO AIM\n RELEASE TO DROP";

		if (m_animateIn) {
			foreach (GameObject btn in m_listButtons) {
				btn.gameObject.SetActive (false);
			}

			foreach (GameObject btn in m_listLeftObjects) {
				//btn.gameObject.SetActive (false);
				//btn.transform.localPosition -= new Vector3(30, 0, 0);
			LeanTween.cancel(btn);
				LeanTween.moveLocal (btn, new Vector3(m_currentLeftGroupPosition - 150, btn.transform.localPosition.y, btn.transform.localPosition.z), 0.8f).setEase (LeanTweenType.easeOutQuint);
			}

			foreach (GameObject btn in m_listRightObjects) {
				//btn.gameObject.SetActive (false);
				//btn.transform.localPosition -= new Vector3(30, 0, 0);
			LeanTween.cancel(btn);
				LeanTween.moveLocal (btn, new Vector3(m_currentRightGroupPosition + 150, btn.transform.localPosition.y, btn.transform.localPosition.z), 0.8f).setEase (LeanTweenType.easeOutQuint);
			}

			m_objectWatchVideo.SetActive (false);
			m_animateIn = false;
		}


		SpawnEnemyMain ();
		m_eState = GAME_STATE.IDLE;
		//m_textLevel.text = "";
		m_textScore.color = m_colorTextCorrect;
		//m_objectTulong.SetActive (false);
		//m_textHit.gameObject.SetActive (false);
		//Play ();
		m_objectArrowMock.SetActive (true);

		//return;
		
		
		

	}

	public void ChangeMode()
	{
		m_currentMode++;
		if (m_currentMode >= ZGameMgr.instance.m_listLeaderboard.Count) {
			m_currentMode = 0;
		}
		//MTCharacter.instance.m_currentMode = m_currentMode;

		RefreshMode ();

		ZAnalytics.Instance.SendModesButton ();

	/*
		if (m_currentMode == 3) {
			MTCharacter.instance.PoopRun ();
		} else {
			MTCharacter.instance.Run (4);
		}*/
		//MTCharacter.instance.Reset ();

		m_textModes.text = ZGameMgr.instance.m_listLeaderboard [m_currentMode].name + " MODE";
		m_textModes2.text = ZGameMgr.instance.m_listLeaderboard [m_currentMode].name + " MODE";

		

		PlayerPrefs.SetInt ("Mode", m_currentMode);

		ResetModeText ();

		SetupLevel ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	void ResetModeText()
	{
		if (m_currentMode == 0)
			m_textLevel.text = "Level";
		else if (m_currentMode == 1)
			m_textLevel.text = "Highscore";
		else if (m_currentMode == 2)
			m_textLevel.text = "Highscore";
		else
			m_textLevel.text = "Level";
	}

	public void RemoveAds()
	{
		ZIAPMgr.Instance.PurchaseRemoveAds ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		ZAnalytics.Instance.SendNoAdsButton ();
	}

	public void FollowUs()
	{
		#if UNITY_ANDROID
		Application.OpenURL (ZGameMgr.instance.FOLLOWUS_URL_ANDROID);
		#else
		Application.OpenURL (ZGameMgr.instance.FOLLOWUS_URL_IOS);
		#endif

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public string ScreenshotName = "archereagle_screenshot.png";
	string m_screenshotText;
	string m_screenshotPath;

	public void ShareScreenshotWithText(string text)
	{
		m_screenshotText = text;
		m_screenshotPath = Application.persistentDataPath + "/" + ScreenshotName;
		Application.CaptureScreenshot(ScreenshotName);

		//LeanTween.delayedCall(1f,ShareScreenshotRoutine);
		StartCoroutine("ScreenshotWriteCheck");
	}

	IEnumerator ScreenshotWriteCheck()
	{
		while (true == true) {
			if (System.IO.File.Exists(m_screenshotPath)) {
				ShareScreenshotRoutine ();
				yield return null;
			} else {
				yield return new WaitForSeconds (0.5f);
			}
		}
	}

	void ShareScreenshotRoutine ()
	{
		#if UNITY_ANDROID
		StartCoroutine(AndroidShare (m_screenshotText, m_screenshotPath, "", ZGameMgr.instance.GAME_HASHTAG));
		#else
		Share (m_screenshotText, m_screenshotPath, "", ZGameMgr.instance.GAME_HASHTAG);
		#endif 
		StopCoroutine("ScreenshotWriteCheck");

	}

	private IEnumerator AndroidShare(string shareText, string imagePath, string url, string subject = "")
	{
		AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
		AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

		intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
		AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
		AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + imagePath);
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
		intentObject.Call<AndroidJavaObject>("setType", "image/png");
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), shareText);

		AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

		AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, subject);
		currentActivity.Call("startActivity", jChooser);

		return null;
	}

	public void Share(string shareText, string imagePath, string url, string subject = "")
	{
		#if UNITY_ANDROID
		/*AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
		AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

		intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
		AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
		AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + imagePath);
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
		intentObject.Call<AndroidJavaObject>("setType", "image/png");

		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), shareText);

		AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

		AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, subject);
		currentActivity.Call("startActivity", jChooser);*/
		#elif UNITY_IOS
		CallSocialShareAdvanced(shareText, subject, url, imagePath);
		#else
		Debug.Log("No sharing set up for this platform.");
		#endif
	}

	#if UNITY_IOS
	public struct ConfigStruct
	{
		public string title;
		public string message;
	}

	[DllImport ("__Internal")] private static extern void showAlertMessage(ref ConfigStruct conf);

	public struct SocialSharingStruct
	{
		public string text;
		public string url;
		public string image;
		public string subject;
	}

	[DllImport ("__Internal")] private static extern void showSocialSharing(ref SocialSharingStruct conf);

	public static void CallSocialShare(string title, string message)
	{
		ConfigStruct conf = new ConfigStruct();
		conf.title  = title;
		conf.message = message;
		showAlertMessage(ref conf);
	}

	public static void CallSocialShareAdvanced(string defaultTxt, string subject, string url, string img)
	{
		SocialSharingStruct conf = new SocialSharingStruct();
		conf.text = defaultTxt; 
		conf.url = url;
		conf.image = img;
		conf.subject = subject;

		showSocialSharing(ref conf);
	}
	#endif

	public void SharePhoto()
	{
		ShareScreenshotWithText ("");

		ZAnalytics.Instance.SendShareButton ();
	}

	public void OpenLeaderboard()
	{
		#if UNITY_ANDROID
		/*if (m_currentMode == 0) {
			ZPlatformCenterMgr.Instance.ShowLeaderboardUI(ZGameMgr.instance.LEADERBOARD_ANDROID);
		}
		else if (m_currentMode == 1) {
			ZPlatformCenterMgr.Instance.ShowLeaderboardUI("CgkI85r1lMsYEAIQAg");
		}
		else if (m_currentMode == 2) {
			ZPlatformCenterMgr.Instance.ShowLeaderboardUI("CgkI85r1lMsYEAIQAw");
		}*/
		ZPlatformCenterMgr.Instance.ShowLeaderboardUI(ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardAndroid);
		#else
		ZPlatformCenterMgr.Instance.ShowLeaderboardUI();
		#endif // UNITY_ANDROID
	
	}

	public void LikeUs()
	{
		//ZPlatformCenterMgr.Instance.Login ();
		//return;
		//ShareScreenshotWithText ("HELLO");
		//ZPlatformCenterMgr.Instance.PostScore(10);
		//return;

		#if UNITY_ANDROID
		Application.OpenURL (ZGameMgr.instance.LIKEUS_URL_ANDROID);
		#else
		Application.OpenURL (ZGameMgr.instance.LIKEUS_URL_IOS);
		#endif

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public GameObject m_objectShop;

	public void ShowShop()
	{
		//ZGameMgr.instance.ShowScene ("ShopScene");
		m_objectShop.SetActive(true);
		ShopScene.instance.SetupScene();
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		
		ZAnalytics.Instance.SendShopButton ();
	}

	public void RateUs()
	{
		#if UNITY_ANDROID
		Application.OpenURL (ZGameMgr.instance.RATEUS_URL_ANDROID);
		#else
		MNIOSNative.RedirectToAppStoreRatingPage(ZGameMgr.instance.RATEUS_URL_IOS);
		#endif

		ZAnalytics.Instance.SendRateUsButton ();
	}

	public void BuyCoins()
	{
		ZIAPMgr.Instance.PurchaseCoins ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);


	}

	public void Resume(){
		ZGameMgr.instance.Resume ();
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public void AddCoins(int index)
	{
		m_currentHits += index;
		PlayerPrefs.SetInt ("Hit", m_currentHits);
	
	m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
	m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;

		/*if (m_currentHits >= GetCurrentShopPrice () && m_eState == GAME_STATE.START && m_currentPurchaseCount < m_listShopBirds.Count) {
			LeanTween.delayedCall (0.2f, AddCoinRoutine);
		}*/
	}

	void AddCoinRoutine()
	{
		ZRewardsMgr.instance.ShowShopButton (true);
		LeanTween.delayedCall (1f, ShowShopAvailable);
	}

	public void RestoreAds()
	{
		//ShareScreenshotWithText ("HELLO");
		//ZPlatformCenterMgr.Instance.ShowLeaderboardUI();
		//return;

		ZIAPMgr.Instance.RestorePurchases ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		ZAnalytics.Instance.SendRestoreIAPButton ();
	}

	public void MoreGames()
	{
		#if UNITY_ANDROID
		Application.OpenURL (ZGameMgr.instance.MOREGAMES_URL_ANDROID);
		#else
		Application.OpenURL (ZGameMgr.instance.MOREGAMES_URL_IOS);
		#endif

		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		ZAnalytics.Instance.SendMoreGamesButton ();
	}

	void SpawnViaMode()
	{
		switch(m_currentMode)
		{
		case 0:
			break;
		case 1: 
			break;
		case 2:
			break;
		}

		//m_objectHeadshot.SetActive (false);
	}

	public List<GameObject> m_listBirds;



	public void UseBird(int count)
	{
		if (PlayerPrefs.GetInt ("birdbought" + count) > 0) {
			m_currentBird = count;
			m_objectShop.SetActive (false);
			PlayerPrefs.SetInt ("currentBird", m_currentBird);
			ZAudioMgr.Instance.PlaySFX (m_audioButton);
			ZAnalytics.Instance.SendUseChar (count);
		} else {
			BuyBird (count);
			ZAudioMgr.Instance.PlaySFX (m_audioBuy);
		}
	}

	public int GetCurrentShopPrice()
	{
		return m_listPrices [m_currentPurchaseCount];
	}

	public void BuyBird(int count)
	{
		if (m_currentHits >= m_listPrices[m_currentPurchaseCount] && PlayerPrefs.GetInt("birdbought" + count) == 0) {
			m_currentHits -= m_listPrices[m_currentPurchaseCount];
			PlayerPrefs.SetInt ("Hit", m_currentHits);
			PlayerPrefs.SetInt ("birdbought" + count, 1);

			ZAnalytics.Instance.SendSpendShopItemEvent ("birdbought" + count, m_listPrices [m_currentPurchaseCount]);
			ZAnalytics.Instance.SendCharComplete ("char", m_currentPurchaseCount);
			
			m_currentPurchaseCount++;
			PlayerPrefs.SetInt ("currentPurchaseCount", m_currentPurchaseCount);
			PlayerPrefs.Save ();

			//UseBird(count);
	m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
	m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
			//ZAudioMgr.Instance.PlaySFX (m_audioButton);

			ShopScene.instance.SetupScene (true, true);
		}
	}

	public void BuyBall(int count)
	{
		if (m_currentHits >= 250  && PlayerPrefs.GetInt("ballbought" + count) == 0) {
			m_currentHits -= 250;
			PlayerPrefs.SetInt ("Hit", m_currentHits);
			PlayerPrefs.SetInt ("ballbought" + count, 1);

			ZAnalytics.Instance.SendSpendShopItemEvent ("ballbought" + count, m_listPrices [m_currentPurchaseCount]);
			ZAnalytics.Instance.SendCharComplete ("ball", m_currentPurchaseCount);

			//m_currentPurchaseCount++;
			//PlayerPrefs.SetInt ("currentPurchaseCount", m_currentPurchaseCount);
			//PlayerPrefs.Save ();

			//UseBird(count);
			m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
			m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
	//ZAudioMgr.Instance.PlaySFX (m_audioButton);randomVerticalPosition

			ShopScene.instance.SetupScene ();
		}

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public void SpawnEnemy(int direction = 0, bool isPlacard = false, bool forceFast = false)
	{
		m_currentSpawnCount++;
		GameObject obj;
		
		// TUNING
		int randomDirection = Random.Range (0, 100);
		float minScale = 1f; 
		float maxScale = 1f;
		float minSpeed = 1f;
		float maxSpeed = 2f;
		float minVerticalSpeed = 0;
		float maxVerticalSpeed = 0;

		//int LEVEL_1_VALUE = 5;
		int LEVEL_2_VALUE = 3;
		int LEVEL_3_VALUE = 10;
		int LEVEL_4_VALUE = 15;
		int LEVEL_5_VALUE = 20;
		int LEVEL_6_VALUE = 25;
		int LEVEL_7_VALUE = 30;
		int LEVEL_8_VALUE = 35;
		int LEVEL_9_VALUE = 40;
		int LEVEL_10_VALUE = 50;

		float birdPosition = 0;

		//if (m_currentMode == 1) {
		//	m_currentLevel = m_currentScore + Mathf.FloorToInt((m_currentHighscore/2f));
		//}

		if (m_currentLevel > LEVEL_10_VALUE) {
			maxSpeed = 2.5f;
			minScale = 0.7f;
			maxScale = 0.8f;
			minSpeed = 0.9f;

			minVerticalSpeed = 0.0f;
			maxVerticalSpeed = 1f;
			int randomBirdPosition = Random.Range (0, 100);
			if (randomBirdPosition > 60) {
				birdPosition = 4;
			} else {
				birdPosition = 0;
			}
		} else if (m_currentLevel > LEVEL_9_VALUE) {
			maxSpeed = 2.4f;
			minScale = 0.7f;
			maxScale = 0.9f;
			minSpeed = 0.9f;

			minVerticalSpeed = 0.0f;
			maxVerticalSpeed = 1f;
			int randomBirdPosition = Random.Range (0, 100);
			if (randomBirdPosition > 68) {
				birdPosition = 4;
			} else {
				birdPosition = 0;
			}
		} else if (m_currentLevel > LEVEL_8_VALUE) {
			maxSpeed = 2.3f;
			minScale = 0.7f;
			maxScale = 0.9f;
			minSpeed = 0.9f;

			minVerticalSpeed = 0.0f;
			maxVerticalSpeed = 1f;
			int randomBirdPosition = Random.Range (0, 100);
			if (randomBirdPosition > 75) {
				birdPosition = 4;
			} else {
				birdPosition = 0;
			}
		} else if (m_currentLevel > LEVEL_7_VALUE) {
			maxSpeed = 2.2f;
			minScale = 0.8f;
			maxScale = 1.0f;
			minSpeed = 0.9f;

			minVerticalSpeed = 0.0f;
			maxVerticalSpeed = 1f;

			int randomBirdPosition = Random.Range (0, 100);
			if (randomBirdPosition > 90) {
				birdPosition = 4;
			} else {
				birdPosition = 0;
			}
		}  else if (m_currentLevel > LEVEL_6_VALUE) {
			maxSpeed = 2.1f;
			minScale = 0.8f;
			maxScale = 1.0f;
			minSpeed = 0.9f;

			minVerticalSpeed = 0.0f;
			maxVerticalSpeed = 1f;

			birdPosition = 0;
			int randomBirdPosition = Random.Range (0, 100);
			if (randomBirdPosition > 90) {
				birdPosition = 4;
			} else {
				birdPosition = 0;
			}
		} else if (m_currentLevel > LEVEL_5_VALUE) {
			maxSpeed = 2f;
			minScale = 0.8f;
			maxScale = 1f;
			minSpeed = 0.9f;

			minVerticalSpeed = 0.0f;
			maxVerticalSpeed = 0.75f;

			birdPosition = 0;
			int randomBirdPosition = Random.Range (0, 100);
			if (randomBirdPosition > 90) {
				birdPosition = 4;
			} else {
				birdPosition = 0;
			}
		} 
		else if (m_currentLevel > LEVEL_4_VALUE) {
			maxSpeed = 1.9f;
			minScale = 0.8f;
			maxScale = 1f;
			minSpeed = 0.5f;

			minVerticalSpeed = 0.0f;
			maxVerticalSpeed = 0.5f;

			birdPosition = 0;
			int randomBirdPosition = Random.Range (0, 100);
			if (randomBirdPosition > 90) {
				birdPosition = 4;
			} else {
				birdPosition = 0;
			}
		} else if (m_currentLevel > LEVEL_3_VALUE) {
			maxSpeed = 1.8f;
			minScale = 0.8f;
			maxScale = 1f;
			minSpeed = 0.5f;

			minVerticalSpeed = 0.0f;
			maxVerticalSpeed = 0.5f;

			birdPosition = 0;
		}
		else if (m_currentLevel > LEVEL_2_VALUE) {
			maxSpeed = 1.7f;
			minScale = 0.8f;
			maxScale = 1f;
			minSpeed = 0.5f;

			minVerticalSpeed = 0;
			maxVerticalSpeed = 0;

			birdPosition = 0;
		} else {
			maxSpeed = 1.4f;
			minScale = 0.9f;
			maxScale = 1.2f;
			minSpeed = 0.5f;

			minVerticalSpeed = 0;
			maxVerticalSpeed = 0;

			birdPosition = 0;
		}

		if (m_currentScore == m_currentLevel) {
			minSpeed = 1f;
			maxSpeed = 1f;
		} else if (forceFast) {
			minSpeed = maxSpeed - 0.2f;
		}else if (isPlacard) {
			maxSpeed = minSpeed + 0.1f;
		} 

		

		/*if (m_currentMode == 1) {
			if (minSpeed <= 2f)
				minSpeed = 2f;
			//maxSpeed *= 1f;
			maxSpeed *= 1.2f;
		}
		else if (m_currentMode == 3) {
			//if( maxSpeed > 3f )
			//	maxSpeed = 3f;
			/*maxSpeed *= 0.8f;

			minScale *= 1.3f;
			if (minScale >= 1.2f)
				minScale = 1.2f;
			maxScale *= 1.3f;
			if (maxScale >= 1.2f)
				maxScale = 1.2f;*/
			//if (minSpeed <= 2f)
			//	minSpeed = 2f;
			//maxSpeed *= 1f;
			//maxSpeed *= 1.2f;
		//}*/

		float randomVerticalPosition = Random.Range (-1f, 3f);
		Vector3 verticalPosition = new Vector3 (0, randomVerticalPosition + birdPosition, randomVerticalPosition);

		float randomSpeedMultiplier = Random.Range (minSpeed, maxSpeed);
		float randomVerticalSpeedMultiplier = Random.Range (minVerticalSpeed, maxVerticalSpeed);

		float verticalSpeedDirectionMultiplier = 1;
		if (randomVerticalPosition < 1) {
			verticalSpeedDirectionMultiplier = -1;
		} else {
			verticalSpeedDirectionMultiplier = 1;
		}

		if (randomSpeedMultiplier < 1.2f) {
			verticalSpeedDirectionMultiplier = 0;
		} 
		
		/*if (m_currentMode == 1) {
			if (direction > 0) {
				obj = ZObjectMgr.Instance.Spawn2D (m_prefabEnemy.name, m_rootSpawnLocationRight.transform.position + verticalPosition);
	obj.transform.localPosition = m_rootSpawnLocationRight.transform.position + verticalPosition;
				obj.GetComponent<MTEnemy> ().Reset (1, randomSpeedMultiplier);
				obj.GetComponent<MTEnemy> ().SetChant (m_currentBird);
				obj.GetComponent<MTEnemy> ().SetMinMaxScale (minScale, maxScale);
			} else {
				obj = ZObjectMgr.Instance.Spawn2D (m_prefabEnemy.name, m_rootSpawnLocationLeft.transform.position + verticalPosition);
	obj.transform.localPosition = m_rootSpawnLocationLeft.transform.position + verticalPosition;
				obj.GetComponent<MTEnemy> ().Reset (-1, randomSpeedMultiplier);
				obj.GetComponent<MTEnemy> ().SetChant (m_currentBird);
				obj.GetComponent<MTEnemy> ().SetMinMaxScale (minScale, maxScale);
			}
		} else {*/
		if (direction == 0) {
			if (randomDirection > 50) {
				obj = ZObjectMgr.Instance.Spawn2D (m_prefabEnemy.name, m_rootSpawnLocationRight.transform.position + verticalPosition);
				obj.transform.localPosition = m_rootSpawnLocationRight.transform.position + verticalPosition;
				obj.GetComponent<MTEnemy> ().Reset (1, randomSpeedMultiplier, randomVerticalSpeedMultiplier * verticalSpeedDirectionMultiplier, true);
				obj.GetComponent<MTEnemy> ().SetChant (m_currentBird);
				//obj.GetComponent<MTEnemy> ().SetMinMaxScale (minScale, maxScale);
			} else {
				obj = ZObjectMgr.Instance.Spawn2D (m_prefabEnemy.name, m_rootSpawnLocationLeft.transform.position + verticalPosition);
				obj.transform.localPosition = m_rootSpawnLocationLeft.transform.position + verticalPosition;
				obj.GetComponent<MTEnemy> ().Reset (-1, randomSpeedMultiplier, randomVerticalSpeedMultiplier * verticalSpeedDirectionMultiplier, true);
				obj.GetComponent<MTEnemy> ().SetChant (m_currentBird);
				//obj.GetComponent<MTEnemy> ().SetMinMaxScale (minScale, maxScale);
			}
		} else {
			if (direction == 1) {
				obj = ZObjectMgr.Instance.Spawn2D (m_prefabEnemy.name, m_rootSpawnLocationRight.transform.position + verticalPosition);
				obj.transform.localPosition = m_rootSpawnLocationRight.transform.position + verticalPosition;
				obj.GetComponent<MTEnemy> ().Reset (1, randomSpeedMultiplier, randomVerticalSpeedMultiplier * verticalSpeedDirectionMultiplier, true);
				obj.GetComponent<MTEnemy> ().SetChant (m_currentBird);
			} else {
				obj = ZObjectMgr.Instance.Spawn2D (m_prefabEnemy.name, m_rootSpawnLocationLeft.transform.position + verticalPosition);
				obj.transform.localPosition = m_rootSpawnLocationLeft.transform.position + verticalPosition;
				obj.GetComponent<MTEnemy> ().Reset (-1, randomSpeedMultiplier, randomVerticalSpeedMultiplier * verticalSpeedDirectionMultiplier, true);
				obj.GetComponent<MTEnemy> ().SetChant (m_currentBird);
			}
		}
		//}
		obj.GetComponent<MTEnemy> ().m_currentMode = m_currentMode;
		
		if (randomSpeedMultiplier >= 1.4f) {
			obj.GetComponent<MTEnemy> ().SetAnimState (MTENEMY_ANIM_STATES.SKATEBOARD);
			obj.GetComponent<Animator> ().enabled = false;
			randomSpeedMultiplier = Random.Range (maxSpeed + 0.2f, maxSpeed);
			obj.GetComponent<MTEnemy> ().m_moveSpeedMultiplier = randomSpeedMultiplier;
			//obj.GetComponent<MTEnemy> ().Reset (-1, randomSpeedMultiplier);
		} else {
			obj.GetComponent<MTEnemy> ().SetAnimState (MTENEMY_ANIM_STATES.RUNNING);
			obj.GetComponent<Animator> ().enabled = true;
		}

		if (isPlacard) {
			obj.GetComponent<MTEnemy> ().SetPlackard (true);
			//obj.GetComponent<Animator> ().enabled = true;
		}
		if (birdPosition > 0) {
			obj.GetComponent<MTEnemy> ().SetGriphon (true);
			obj.GetComponent<Animator> ().enabled = false;
		}
		//if( m_currentMode == 0 || m_currentMode == 2)
		//	LeanTween.delayedCall (Random.Range (1.4f, 2.5f), SpawnEnemy);
	/*
	if (m_currentLevel > 30)
	MTCharacter.instance.SetMaxSpeed (2.5f);
	else if (m_currentLevel > 20)
	MTCharacter.instance.SetMaxSpeed (2.2f);
	else if (m_currentLevel > 10)
	MTCharacter.instance.SetMaxSpeed (2f);
	else
	MTCharacter.instance.SetMaxSpeed (1.5f);*/

		m_currentBird = 0;
		int x = 0;
		foreach (ShopBird bird in m_listShopBirds) {
			if (PlayerPrefs.GetInt ("birdbought" + x) > 0) {
				if (Random.Range (0, 100) < 30) {
					m_currentBird = x;
				}
			}
			x++;
		}
	//obj.GetComponent<MTEnemy>().m_rendererImage.sprite = m_listShopBirds [m_currentBird].spriteImage;
		obj.GetComponent<Animator> ().runtimeAnimatorController = m_listShopBirds [m_currentBird].animator;

		//m_currentEnemy = obj;
		m_listEnemies.Add (obj);

		/*float speedMultiplier = 1;
		GameObject obj;
		int randomSpawn = Random.Range (0, 100);
		//int randomSpawn = 50;
		/*if (m_currentLevel > 25 && m_currentScore <= 1) {
			obj = ZObjectMgr.Instance.Spawn2D (m_prefabBirdBig.name, m_rootSpawnLocation.transform.position);
			//speedMultiplier = 0.75f;
			speedMultiplier = 0.7f;
		}
		else if (m_currentLevel > 75 && randomSpawn > 35 && randomSpawn < 65) {
			obj = ZObjectMgr.Instance.Spawn2D (m_prefabBirdBig.name, m_rootSpawnLocation.transform.position);
			//speedMultiplier = 0.75f;
			speedMultiplier = 0.7f;
		}
		//else if (m_currentLevel > 50 && m_currentScore < m_currentLevel/2 && randomSpawn > 35 && randomSpawn < 65) {
		else if (m_currentLevel > 50 && randomSpawn > 40 && randomSpawn < 60) {
			obj = ZObjectMgr.Instance.Spawn2D (m_prefabBirdBig.name, m_rootSpawnLocation.transform.position);
			//speedMultiplier = 0.75f;
			speedMultiplier = 0.7f;
		}
		//else if (m_currentLevel > 25 && m_currentScore < m_currentLevel/2 && randomSpawn > 40 && randomSpawn < 60) {
		else */

		/*if (m_currentMode == 2) {
			m_currentLevel = m_currentScore;
		}
			
		float scaleMultipler = 1;
		if (m_currentLevel > 25 && randomSpawn > 45 && randomSpawn < 55) {
			obj = ZObjectMgr.Instance.Spawn2D (m_prefabBirdBig.name, m_rootSpawnLocation.transform.position);
			//speedMultiplier = 0.75f;
			speedMultiplier = 0.7f;
			scaleMultipler = 1.4f;
		} else {
			obj = ZObjectMgr.Instance.Spawn2D (m_prefabBird.name, m_rootSpawnLocation.transform.position);
			obj.GetComponent<Animator> ().runtimeAnimatorController = m_listShopBirds [m_currentBird].animator;
			speedMultiplier = 1;
		}



		float scaleRandom = 1f;
		if (m_currentLevel > 70) {
			obj.GetComponent<Rigidbody2D> ().velocity = new Vector2 (Random.Range (-2f, -8f) * speedMultiplier, Random.Range (-1.5f, 1.5f));
			obj.transform.position += new Vector3 (0, Random.Range (-4f, 4f), 0);
			scaleRandom = Random.Range (0.6f, 0.8f) * scaleMultipler;
		} else if (m_currentLevel > 50) {
			obj.GetComponent<Rigidbody2D> ().velocity = new Vector2 (Random.Range (-2f, -8f) * speedMultiplier, Random.Range (-1f, 1f));
			obj.transform.position += new Vector3 (0, Random.Range (-4f, 4f), 0);
			scaleRandom = Random.Range (0.6f, 0.8f) * scaleMultipler;
		} else if (m_currentLevel > 30) {
			obj.GetComponent<Rigidbody2D> ().velocity = new Vector2 (Random.Range (-2f, -7f) * speedMultiplier, Random.Range (-1f, 1f));
			obj.transform.position += new Vector3 (0, Random.Range (-4f, 4f), 0);
			scaleRandom = Random.Range (0.6f, 0.8f) * scaleMultipler;
		} else if (m_currentLevel > 10) {
			obj.GetComponent<Rigidbody2D> ().velocity = new Vector2 (Random.Range (-2f, -6f) * speedMultiplier, Random.Range (-1f, 1f));
			obj.transform.position += new Vector3 (0, Random.Range (-4f, 4f), 0);
			scaleRandom = Random.Range (0.6f, 0.8f) * scaleMultipler;
		} else if (m_currentLevel > 5) {
			obj.GetComponent<Rigidbody2D> ().velocity = new Vector2 (Random.Range (-2f, -5.4f) * speedMultiplier, 0);
			obj.transform.position += new Vector3 (0, Random.Range (-4f, 3f), 0);
			scaleRandom = Random.Range (0.7f, 0.8f) * scaleMultipler;
		} else if (m_currentLevel == 1) {
			obj.GetComponent<Rigidbody2D> ().velocity = new Vector2 (Random.Range (-2f, -2.5f) * speedMultiplier, 0);
			obj.transform.position += new Vector3(0, Random.Range(-4f, 3f), 0);
			scaleRandom = Random.Range (0.8f, 0.8f) * scaleMultipler;
		} else {
			obj.GetComponent<Rigidbody2D> ().velocity = new Vector2 (Random.Range (-2f, -4f) * speedMultiplier, 0);
			obj.transform.position += new Vector3(0, Random.Range(-4f, 1f), 0);
			scaleRandom = Random.Range (0.8f, 0.8f) * scaleMultipler;
		}

		obj.transform.localEulerAngles = Vector3.zero;
		//obj.transform.localScale = new Vector3 (-0.8f, 0.8f, 0.8f);

		obj.transform.localScale = new Vector3 (-scaleRandom, scaleRandom, scaleRandom);
		obj.GetComponent<Bird> ().Reset ();
		obj.GetComponent<Bird> ().SetChant (m_currentBird);

		m_listBirds.Add (obj);

		m_currentEnemy = obj;*/
	}

	void RefreshMode()
	{	//PlayerPrefs.SetInt (ZGameMgr.instance.m_listLeaderboard [m_currentMode].saveName, m_currentLevel);
		/*if( m_currentMode == 0 )
			m_currentLevel = PlayerPrefs.GetInt ("Level");
			//m_currentLevel = 60;
		else if( m_currentMode == 1 )
			m_currentLevel = PlayerPrefs.GetInt ("LevelQuick");
			//m_currentLevel = 75;
		else if( m_currentMode == 2 )
			m_currentLevel = PlayerPrefs.GetInt ("LevelEndless");
			//m_currentLevel = 75;*/

		m_currentLevel = PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard [m_currentMode].saveName);

		/*if (m_currentMode == 3) {
			m_objectTutorial.GetComponent<Text> ().text = "SWIPE THE BALL UP";
		} else {
			m_objectTutorial.GetComponent<Text> ().text = "SWIPE THE BALL UP";
		}*/
		m_objectTutorial.GetComponent<Text> ().text = "TAP TO PLAY";
		ResetScore ();
	}

	void SpawnBall()
	{return;
		/*if (m_currentLevel > 40) {

		} else if (m_currentLevel > 30) {

		} else if (m_currentLevel > 20) {

		} */
		
		float BALL_POSITION = -6.6f;
		if(m_currentMode != 1 ){
			if (m_eState != GAME_STATE.START && m_currentLevel > 40) {
				m_objectBallRoot.transform.localPosition = new Vector3 (Random.Range (-5, 5), BALL_POSITION + Random.Range (0, 4), 0);
			}else if (m_eState != GAME_STATE.START && m_currentLevel > 30) {
				m_objectBallRoot.transform.localPosition = new Vector3 (Random.Range (-3.5f, 3.5f), BALL_POSITION + Random.Range (0, 2), 0);
			}else if (m_eState != GAME_STATE.START && m_currentLevel > 20) {
				m_objectBallRoot.transform.localPosition = new Vector3 (Random.Range (-3, 3), BALL_POSITION, 0);
			}else if (m_eState != GAME_STATE.START && m_currentLevel > 10) {
				m_objectBallRoot.transform.localPosition = new Vector3 (Random.Range (-2.5f, 2.5f), BALL_POSITION, 0);
			} else if (m_eState != GAME_STATE.START && m_currentLevel > 5) {
				m_objectBallRoot.transform.localPosition = new Vector3 (Random.Range (-2, 2), BALL_POSITION, 0);
			} else {
				m_objectBallRoot.transform.localPosition = new Vector3 (0, BALL_POSITION, 0);
			}
		} else{
			m_objectBallRoot.transform.localPosition = new Vector3 (0, BALL_POSITION, 0);
		}

		m_objectBall = ZObjectMgr.Instance.Spawn2D (m_prefabBall.name, m_objectBallRoot.transform.position);
		
		m_objectBall.GetComponent<Rigidbody2D> ().isKinematic = true;
		m_objectBall.transform.localScale = new Vector3 (0.8f, 0.8f, 0.8f);
		m_objectBall.transform.eulerAngles = Vector3.zero;
		m_objectBall.GetComponent<Ball> ().Reset ();

		if (m_currentMode == 3) {
			m_objectBall.GetComponent<Ball> ().Curve ();
		}

		int x = 0;
		int currentBall = 0;
		foreach (ShopBird bird in m_listShopBalls) {
			if (PlayerPrefs.GetInt ("ballbought" + x) > 0) {
				if (Random.Range (0, 100) < 20) {
					currentBall = x;
				}
			}
			x++;
		}
		m_objectBall.GetComponent<SpriteRenderer>().sprite = m_listShopBalls [currentBall].spriteImage;

	}

	void SetupLevel()
	{
		/*if( m_isWatchVideo == 2 ){
			m_isWatchVideo = 0;
			m_objectWatchVideo.SetActive (true);
			LeanTween.delayedCall (1, SetupLevel);
	ZAudioMgr.Instance.PlaySFX (m_audioButton);SetupLevel(
			return;
		}*/


		ZObjectMgr.Instance.ResetAll ();
		
		//m_objectShop.SetActive(false);

		RefreshMode ();
		ResetModeText ();

		m_textLevel.color = m_colorTextSuccess;
		m_textScore.color = m_colorTextSuccess;
		m_objectMiss.SetActive (false);
		m_objectSuccess.SetActive (false);
		m_objectArrowMock.SetActive (false);
		m_textHit.gameObject.SetActive (true);
		m_objectReadyUnlock.SetActive (false);
		//Debug.LogError ("Setup Level");
		m_objectHeadshot.SetActive (false);
		m_objectParticle.SetActive (false);
		m_objectTarget.SetActive (false);
	//m_objectTutorialFinger.SetActive (false);

		if (m_objectTutorialFinger.GetComponent<SpriteRenderer> ().color.a == 1) {
			LeanTween.alpha (m_objectTutorialFinger, 0, 0.2f);
		}

		m_objectTutorial.SetActive (true);
		m_objectTopBar.SetActive(true);
		//m_objectEarned.SetActive (false);

		m_objectNormal.SetActive (true);
		m_objectWin.SetActive (false);

		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		foreach (GameObject btn in m_listButtons) {
			btn.gameObject.SetActive (true);
		}

		/*if (m_isWatchVideo > 0) {
			//m_objectWatchVideo.SetActive (true);
			m_isWatchVideo = 0;
		} else {
			m_objectWatchVideo.SetActive (false);
		}*/
		//MTCharacter.instance.Reset ();
		m_currentSpawnCount = 0;

		if(m_currentMode == 3 )
			m_isSuccess = false;

		m_eState = GAME_STATE.START;

		if (m_currentLeftGroupPosition == -999) {
			m_currentLeftGroupPosition = m_listLeftObjects [0].transform.localPosition.x;
		}
		if (m_currentRightGroupPosition == -999) {
			m_currentRightGroupPosition = m_listRightObjects [0].transform.localPosition.x;
		}

		if (!m_animateIn) {
			foreach (GameObject btn in m_listButtons) {
				btn.gameObject.SetActive (true);
			}

			

			foreach (GameObject btn in m_listLeftObjects) {
				//btn.gameObject.SetActive (false);
				//btn.transform.localPosition -= new Vector3(30, 0, 0);
				LeanTween.moveLocal (btn, new Vector3(m_currentLeftGroupPosition, btn.transform.localPosition.y, btn.transform.localPosition.z), 0.8f).setEase (LeanTweenType.easeOutQuint);
			}

			foreach (GameObject btn in m_listRightObjects) {
				//btn.gameObject.SetActive (false);
				//btn.transform.localPosition -= new Vector3(30, 0, 0);
				LeanTween.moveLocal (btn, new Vector3(m_currentRightGroupPosition, btn.transform.localPosition.y, btn.transform.localPosition.z), 0.8f).setEase (LeanTweenType.easeOutQuint);
			}

			//m_objectWatchVideo.SetActive (false);
			m_animateIn = true;
		}

		LeanTween.scale (m_objectTrumpsHairHead, new Vector3 (1, 1, 1), 0.1f);
		m_objectTrumpsHairBird.transform.localScale = Vector3.zero;

		//SpawnBall ();
		ZAnalytics.Instance.SendLevelStart ("mode" + m_currentMode, m_currentLevel, m_currentAttempts);

		PlayerPrefs.Save ();
	}

	void ContinueLevel()
	{
		ZObjectMgr.Instance.ResetAll ();

		m_listButtons [0].SetActive (true);
		m_currentSpawnCount = 0;
		m_textLevel.text = "Continue";
		m_textScore.text = "" + m_currentScore;

		m_textLevel.color = m_colorTextSuccess;
		m_textScore.color = m_colorTextSuccess;
		m_objectMiss.SetActive (false);
		m_objectSuccess.SetActive (false);
		m_objectArrowMock.SetActive (false);
		m_textHit.gameObject.SetActive (true);
		//Debug.LogError ("Setup Level");

		m_objectParticle.SetActive (false);
		m_objectTarget.SetActive (false);

		m_objectTutorial.SetActive (true);
		//MTCharacter.instance.m_currentState = MTCHAR_STATES.IDLE;

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		ZAnalytics.Instance.SendSkipComplete ();

		m_animateIn = true;
		m_eState = GAME_STATE.START;
		//SpawnBall ();
	}

	void ResetScore()
	{
		m_textLevel.text = "Level";
		m_currentScore = m_currentLevel;
		m_textScore.text = "" + m_currentScore;
	}

	void ShootArrow()
	{
		/*return;
		GameObject obj = ZObjectMgr.Instance.Spawn2D (m_prefabArrow.name, m_objectCharHead.transform.position - new Vector3(0.1f,0.6f, 0));
		obj.GetComponent<Rigidbody2D> ().isKinematic = false;
		obj.transform.localScale = new Vector3 (0.6f, 0.6f, 0.6f);
		obj.transform.eulerAngles = m_objectCharHead.transform.eulerAngles + new Vector3(0, 0, -90);
		obj.GetComponent<Rigidbody2D> ().AddForce (obj.transform.TransformDirection(new Vector2(0, 1600)));

		obj.transform.parent = m_objectRootArrows.transform;

		obj.GetComponent<BoxCollider2D> ().enabled = true;

		ZAudioMgr.Instance.PlaySFX (m_audioShoot);*/
	}

	/*public override void OnButtonUp(ZButton button)
	{
		if (button.name == "btn_play") {
		}
		else if (button.name == "btn_game") {
		}
		else if (button.name == "btn_pause") {
		}
		else if (button.name == "btn_showad") {
		}
		else if (button.name == "btn_showadvideo") {
		}
	}
	
	public override void OnButtonDown(ZButton button)
	{
		if (button.name == "btn_game") {
		}
	}*/
}
